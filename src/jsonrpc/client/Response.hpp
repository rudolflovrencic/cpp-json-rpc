#ifndef JSONRPC_CLIENT_RESPONSE_HPP
#define JSONRPC_CLIENT_RESPONSE_HPP

#include "jsonrpc/ErrorCode.hpp"
#include "jsonrpc/client/Error.hpp"

#include <boost/json.hpp>

#include <exception>
#include <memory>
#include <string>
#include <variant>

namespace jsonrpc::client {

class Response {
  private:
    boost::json::object json_object;

  public:
    [[nodiscard]] const boost::json::value& get_id() const noexcept;
    [[nodiscard]] boost::json::object& get_json() noexcept;
    [[nodiscard]] const boost::json::object& get_json() const noexcept;

  protected:
    explicit Response(boost::json::object json_object);
    ~Response() noexcept                                = default;
    Response(const Response& other) noexcept            = default;
    Response& operator=(const Response& other) noexcept = default;
    Response(Response&& other) noexcept                 = default;
    Response& operator=(Response&& other) noexcept      = default;

  private:
    void validate_jsonrpc_member();
    void validate_id_member();
};

class SuccessResponse : public Response {
  public:
    explicit SuccessResponse(boost::json::object success_response);

    [[nodiscard]] const boost::json::value& get_result() const noexcept;
    [[nodiscard]] boost::json::value& get_result() noexcept;
};

class ErrorResponse : public Response {
  public:
    explicit ErrorResponse(boost::json::object error_response);

    [[nodiscard]] ErrorCode get_error_code() const noexcept;

    [[nodiscard]] const boost::json::string& get_error_message() const noexcept;
    [[nodiscard]] boost::json::string& get_error_message() noexcept;

    [[nodiscard]] const boost::json::value* get_error_data() const noexcept;
    [[nodiscard]] boost::json::value* get_error_data() noexcept;

  private:
    [[nodiscard]] const boost::json::object& get_error_object() const noexcept;
    [[nodiscard]] boost::json::object& get_error_object() noexcept;

    void validate_error_member();
};

[[nodiscard]] std::vector<std::variant<SuccessResponse, ErrorResponse>> parse_response(std::string_view response);
[[nodiscard]] std::vector<std::variant<SuccessResponse, ErrorResponse>> parse_response(boost::json::value response);
[[nodiscard]] std::variant<SuccessResponse, ErrorResponse> parse_response(boost::json::object response);

class ResponseStringParseError final : public Error {
  private:
    std::shared_ptr<std::string> response_string;

  public:
    explicit ResponseStringParseError(std::string response_string) noexcept;
    [[nodiscard]] const std::string& get_response_string() const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<ResponseStringParseError>);

class InvalidResponseKindError final : public Error {
  public:
    static constexpr boost::json::kind expected_kind{boost::json::kind::object};

  private:
    std::shared_ptr<boost::json::value> response_json_value;

  public:
    explicit InvalidResponseKindError(boost::json::value response_json_value) noexcept;
    boost::json::kind get_response_kind() const noexcept;
    const boost::json::value& get_response_json_value() const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidResponseKindError>);

class InvalidResponseArrayMemberKindError final : public Error {
  public:
    static constexpr boost::json::kind expected_kind{boost::json::kind::object};

  private:
    std::shared_ptr<boost::json::array> response_json_array;
    std::size_t invalid_member_index;

  public:
    InvalidResponseArrayMemberKindError(boost::json::array response_json_array,
                                        std::size_t invalid_member_index) noexcept;
    std::size_t get_invalid_member_index() const noexcept;
    boost::json::kind get_invalid_member_kind() const noexcept;
    const boost::json::array& get_response_json_array() const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidResponseArrayMemberKindError>);

class EmptyBatchResponseError final : public Error {
  public:
    explicit EmptyBatchResponseError() noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<EmptyBatchResponseError>);

class ResponseFormatError : public Error {
  private:
    std::shared_ptr<boost::json::object> response;

  public:
    ~ResponseFormatError() noexcept override = default;
    [[nodiscard]] boost::json::object& get_response() const noexcept;

  protected:
    [[nodiscard]] explicit ResponseFormatError(boost::json::object response) noexcept;
    ResponseFormatError(const ResponseFormatError& other) noexcept            = default;
    ResponseFormatError& operator=(const ResponseFormatError& other) noexcept = default;
    ResponseFormatError(ResponseFormatError&& other) noexcept                 = default;
    ResponseFormatError& operator=(ResponseFormatError&& other) noexcept      = default;
};

class MissingJsonRpcMemberResponseError final : public ResponseFormatError {
  public:
    explicit MissingJsonRpcMemberResponseError(boost::json::object response) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<MissingJsonRpcMemberResponseError>);

class InvalidJsonRpcMemberKindResponseError final : public ResponseFormatError {
  public:
    static constexpr boost::json::kind expected_kind{boost::json::kind::string};

  public:
    explicit InvalidJsonRpcMemberKindResponseError(boost::json::object response) noexcept;
    [[nodiscard]] boost::json::kind get_json_rpc_member_kind() const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidJsonRpcMemberKindResponseError>);

class InvalidJsonRpcMemberValueResponseError final : public ResponseFormatError {
  public:
    static constexpr std::string_view expected_value{"2.0"};

  public:
    explicit InvalidJsonRpcMemberValueResponseError(boost::json::object response) noexcept;
    [[nodiscard]] std::string_view get_json_rpc_member_value() const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidJsonRpcMemberValueResponseError>);

class MissingIdMemberResponseError final : public ResponseFormatError {
  public:
    explicit MissingIdMemberResponseError(boost::json::object response) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<MissingIdMemberResponseError>);

class InvalidIdMemberKindResponseError final : public ResponseFormatError {
  public:
    explicit InvalidIdMemberKindResponseError(boost::json::object response) noexcept;
    [[nodiscard]] boost::json::value& get_id() const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidIdMemberKindResponseError>);

class MissingResultAndErrorMembersResponseError final : public ResponseFormatError {
  public:
    explicit MissingResultAndErrorMembersResponseError(boost::json::object response) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<MissingResultAndErrorMembersResponseError>);

class ContainingResultAndErrorMembersResponseError final : public ResponseFormatError {
  public:
    explicit ContainingResultAndErrorMembersResponseError(boost::json::object response) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<ContainingResultAndErrorMembersResponseError>);

class InvalidErrorMemberKindResponseError final : public ResponseFormatError {
  public:
    static constexpr boost::json::kind expected_kind{boost::json::kind::object};

  public:
    explicit InvalidErrorMemberKindResponseError(boost::json::object response) noexcept;
    [[nodiscard]] boost::json::kind get_error_member_kind() const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidErrorMemberKindResponseError>);

class ResponseErrorObjectFormatError : public ResponseFormatError {
  protected:
    explicit ResponseErrorObjectFormatError(boost::json::object response) noexcept;
    [[nodiscard]] boost::json::object& get_error_object() const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<ResponseErrorObjectFormatError>);

class ErrorObjectMissingCodeMemberResponseError final : public ResponseErrorObjectFormatError {
  public:
    explicit ErrorObjectMissingCodeMemberResponseError(boost::json::object response) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<ErrorObjectMissingCodeMemberResponseError>);

class ErrorObjectInvalidCodeMemberKindResponseError final : public ResponseErrorObjectFormatError {
  public:
    explicit ErrorObjectInvalidCodeMemberKindResponseError(boost::json::object response) noexcept;
    [[nodiscard]] boost::json::kind get_error_code_member_kind() const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<ErrorObjectInvalidCodeMemberKindResponseError>);

class ErrorObjectMissingMessageMemberResponseError final : public ResponseErrorObjectFormatError {
  public:
    explicit ErrorObjectMissingMessageMemberResponseError(boost::json::object response) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<ErrorObjectMissingMessageMemberResponseError>);

class ErrorObjectInvalidMessageMemberKindResponseError final : public ResponseErrorObjectFormatError {
  public:
    static constexpr boost::json::kind expected_kind{boost::json::kind::string};

  public:
    explicit ErrorObjectInvalidMessageMemberKindResponseError(boost::json::object response) noexcept;
    [[nodiscard]] boost::json::kind get_error_message_member_kind() const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<ErrorObjectInvalidMessageMemberKindResponseError>);

}

#endif
