#ifndef JSONRPC_SERVER_ERROR_HPP
#define JSONRPC_SERVER_ERROR_HPP

#include "jsonrpc/ErrorCode.hpp"

#include <boost/json.hpp>

#include <string>
#include <memory>
#include <exception>
#include <type_traits>

namespace jsonrpc::server {

boost::json::object create_error_response(ErrorCode error_code,
                                          std::string message,
                                          boost::json::value data = boost::json::value{},
                                          boost::json::value id   = boost::json::value{}) noexcept;

class Error : public std::exception {
  private:
    std::shared_ptr<std::string> what_message;
    ErrorCode error_response_code;
    std::shared_ptr<std::string> error_response_message;

  public:
    ~Error() noexcept override = default;

    [[nodiscard]] ErrorCode get_error_response_code() const noexcept;
    [[nodiscard]] const std::string& get_error_response_message() const noexcept;
    [[nodiscard]] virtual boost::json::value get_error_response_data() const noexcept;
    [[nodiscard]] boost::json::object create_error_response(boost::json::value id = nullptr) const noexcept;
    [[nodiscard]] const char* what() const noexcept final;

  protected:
    Error(ErrorCode error_response_code, std::string error_response_message) noexcept;

    Error(const Error& other) noexcept            = default;
    Error& operator=(const Error& other) noexcept = default;
    Error(Error&& other) noexcept                 = default;
    Error& operator=(Error&& other) noexcept      = default;

    void set_what_message(std::string what_message) const noexcept;
};

}

#endif
