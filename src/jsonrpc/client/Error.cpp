#include "jsonrpc/client/Error.hpp"

namespace jsonrpc::client {

Error::Error() noexcept : what_message{std::make_shared<std::string>()} {}

void Error::set_what_message(std::string what_message) const noexcept
{
    assert(!what_message.empty());
    *this->what_message = std::move(what_message);
}

const char* Error::what() const noexcept
{
    return what_message->c_str();
}

}
