#include <boost/test/unit_test.hpp>

#include "jsonrpc/client/Response.hpp"

BOOST_AUTO_TEST_SUITE(response_parsing);

BOOST_AUTO_TEST_CASE(invalid_json_string_results_in_an_error)
{
    BOOST_CHECK_THROW(const auto response =
                          jsonrpc::client::parse_response(std::string_view{R"({ "key1": "missing_closing_quote })"}),
                      jsonrpc::client::ResponseStringParseError);
}

BOOST_AUTO_TEST_CASE(invalid_response_kind_results_in_an_error)
{
    BOOST_CHECK_THROW(const auto response = jsonrpc::client::parse_response(std::string_view{"42"}),
                      jsonrpc::client::InvalidResponseKindError);
}

BOOST_AUTO_TEST_CASE(invalid_response_array_results_in_an_error)
{
    BOOST_CHECK_THROW(const auto response = jsonrpc::client::parse_response(boost::json::value{"42", 3.14}),
                      jsonrpc::client::InvalidResponseArrayMemberKindError);
}

BOOST_AUTO_TEST_CASE(empty_batch_response_results_in_an_error)
{
    BOOST_CHECK_THROW(const auto response = jsonrpc::client::parse_response(boost::json::array{}),
                      jsonrpc::client::EmptyBatchResponseError);
}

BOOST_AUTO_TEST_SUITE(faulty_jsonrpc_member)

BOOST_AUTO_TEST_CASE(missing_member_results_in_an_error)
{
    BOOST_CHECK_THROW(const auto response = (jsonrpc::client::parse_response(
                          boost::json::object{{"error", "some_invalid_error_object"}})),
                      jsonrpc::client::MissingJsonRpcMemberResponseError);
    BOOST_CHECK_THROW(const auto response = (jsonrpc::client::parse_response(boost::json::object{
                          {"JsonRpc", "2.0"}, {"method", true}, {"error", "some_invalid_error_object"}})),
                      jsonrpc::client::MissingJsonRpcMemberResponseError);
    BOOST_CHECK_THROW(const auto response =
                          (jsonrpc::client::parse_response(boost::json::object{{"JsonRp", "2.0"}, {"result", 42}})),
                      jsonrpc::client::MissingJsonRpcMemberResponseError);
}

BOOST_AUTO_TEST_CASE(invalid_kind_results_in_an_error)
{
    BOOST_CHECK_THROW(const auto response =
                          (jsonrpc::client::parse_response(boost::json::object{{"jsonrpc", false}, {"result", 42}})),
                      jsonrpc::client::InvalidJsonRpcMemberKindResponseError);
    BOOST_CHECK_THROW(const auto response =
                          (jsonrpc::client::parse_response(boost::json::object{{"jsonrpc", 2.0}, {"result", 42}})),
                      jsonrpc::client::InvalidJsonRpcMemberKindResponseError);
}

BOOST_AUTO_TEST_CASE(invalid_value_results_in_an_error)
{
    BOOST_CHECK_THROW(const auto response =
                          (jsonrpc::client::parse_response(boost::json::object{{"jsonrpc", "abc"}, {"result", 42}})),
                      jsonrpc::client::InvalidJsonRpcMemberValueResponseError);
    BOOST_CHECK_THROW(const auto response =
                          (jsonrpc::client::parse_response(boost::json::object{{"jsonrpc", "2.0 "}, {"result", 42}})),
                      jsonrpc::client::InvalidJsonRpcMemberValueResponseError);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_CASE(missing_result_and_error_members_results_in_an_error)
{
    BOOST_CHECK_THROW(const auto response = (jsonrpc::client::parse_response(
                          boost::json::object{{"jsonrpc", "2.0"}, {"id", "stringid"}})),
                      jsonrpc::client::MissingResultAndErrorMembersResponseError);
}

BOOST_AUTO_TEST_CASE(containing_result_and_error_members_results_in_an_error)
{
    BOOST_CHECK_THROW(
        const auto response = (jsonrpc::client::parse_response(boost::json::object{
            {"jsonrpc", "2.0"}, {"result", 42}, {"error", "some_invalid_error_object"}, {"id", "stringid"}})),
        jsonrpc::client::ContainingResultAndErrorMembersResponseError);
}

BOOST_AUTO_TEST_SUITE(faulty_error_member)

BOOST_AUTO_TEST_CASE(invalid_kind_results_in_an_error)
{
    BOOST_CHECK_THROW(const auto response = (jsonrpc::client::parse_response(boost::json::object{
                          {"jsonrpc", "2.0"}, {"error", "some_invalid_error_object"}, {"id", "stringid"}})),
                      jsonrpc::client::InvalidErrorMemberKindResponseError);
}

BOOST_AUTO_TEST_CASE(missing_code_member_results_in_an_error)
{
    BOOST_CHECK_THROW(
        const auto response = (jsonrpc::client::parse_response(boost::json::object{
            {"jsonrpc", "2.0"}, {"error", boost::json::object{{"message", "some message"}}}, {"id", "stringid"}})),
        jsonrpc::client::ErrorObjectMissingCodeMemberResponseError);
}

BOOST_AUTO_TEST_CASE(invalid_code_member_kind_results_in_an_error)
{
    BOOST_CHECK_THROW(
        const auto response = (jsonrpc::client::parse_response(boost::json::object{
            {"jsonrpc", "2.0"}, {"error", {{"code", 3.14}, {"message", "some message"}}}, {"id", "stringid"}})),
        jsonrpc::client::ErrorObjectInvalidCodeMemberKindResponseError);
}

BOOST_AUTO_TEST_CASE(missing_message_member_results_in_an_error)
{
    BOOST_CHECK_THROW(const auto response = (jsonrpc::client::parse_response(boost::json::object{
                          {"jsonrpc", "2.0"}, {"error", boost::json::object{{"code", 42}}}, {"id", "stringid"}})),
                      jsonrpc::client::ErrorObjectMissingMessageMemberResponseError);
}

BOOST_AUTO_TEST_CASE(invalid_message_member_kind_results_in_an_error)
{
    BOOST_CHECK_THROW(
        const auto response = (jsonrpc::client::parse_response(boost::json::object{
            {"jsonrpc", "2.0"}, {"error", {{"code", 42}, {"message", boost::json::array{}}}}, {"id", "stringid"}})),
        jsonrpc::client::ErrorObjectInvalidMessageMemberKindResponseError);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(faulty_id_member)

BOOST_AUTO_TEST_CASE(invalid_kind_results_in_an_error)
{
    BOOST_CHECK_THROW(const auto response = (jsonrpc::client::parse_response(
                          boost::json::object{{"jsonrpc", "2.0"}, {"result", 42}, {"id", boost::json::object{}}})),
                      jsonrpc::client::InvalidIdMemberKindResponseError);
    BOOST_CHECK_THROW(const auto response = (jsonrpc::client::parse_response(
                          boost::json::object{{"jsonrpc", "2.0"}, {"result", 42}, {"id", boost::json::array{}}})),
                      jsonrpc::client::InvalidIdMemberKindResponseError);
    BOOST_CHECK_THROW(const auto response = (jsonrpc::client::parse_response(
                          boost::json::object{{"jsonrpc", "2.0"}, {"result", 42}, {"id", true}})),
                      jsonrpc::client::InvalidIdMemberKindResponseError);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE_END();
