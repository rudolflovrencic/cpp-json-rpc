#include "jsonrpc/server/Request.hpp"

#include <sstream>
#include <iomanip>

namespace jsonrpc::server {

Request::Request(boost::json::object request) : request{std::move(request)}
{
    validate_jsonrpc_member();
    validate_method_member();
    validate_params_member();
    validate_id_member();
}

const boost::json::string& Request::get_method() const noexcept
{
    return *request.if_contains("method")->if_string();
}

const boost::json::value* Request::get_params() const noexcept
{
    return request.if_contains("params");
}

const boost::json::value* Request::get_id() const noexcept
{
    return request.if_contains("id");
}

std::size_t Request::get_number_of_parameters() const noexcept
{
    const auto* const params = get_params();
    if (!params) { return 0; }

    assert(params->is_array() || params->is_object());
    return params->is_array() ? params->get_array().size() : params->get_object().size();
}

const boost::json::object& Request::get_json() const& noexcept
{
    return request;
}

boost::json::object&& Request::get_json() && noexcept
{
    return std::move(request);
}

void Request::validate_jsonrpc_member()
{
    const auto jsonrpc_it = request.find("jsonrpc");
    if (jsonrpc_it == request.end()) { throw MissingJsonRpcMemberRequestError{std::move(request)}; }
    const auto* const jsonrpc = jsonrpc_it->value().if_string();
    if (!jsonrpc) { throw InvalidJsonRpcMemberKindError{std::move(request)}; }
    if (*jsonrpc != InvalidJsonRpcMemberValueError::expected_value) {
        throw InvalidJsonRpcMemberValueError{std::move(request)};
    }
}

void Request::validate_method_member()
{
    const auto method_it = request.find("method");
    if (method_it == request.end()) { throw MissingMethodMemberRequestError{std::move(request)}; }
    const auto* const method = method_it->value().if_string();
    if (!method) { throw InvalidMethodMemberKindError{std::move(request)}; }
    if (method->empty()) { throw EmptyMethodMemberError{std::move(request)}; }
}

void Request::validate_params_member()
{
    const auto params_it = request.find("params");
    if (params_it == request.end()) { return; }

    const auto& params = params_it->value();
    if (!params.is_array() && !params.is_object()) { throw InvalidParamsMemberKindError{std::move(request)}; }
}

void Request::validate_id_member()
{
    const auto id_it = request.find("id");
    if (id_it == request.end()) { return; }

    const auto& id = id_it->value();
    if (!id.is_string() && !id.is_number() && !id.is_null()) { throw InvalidIdMemberKindError{std::move(request)}; }
}

boost::json::object& RequestFormatError::get_request() const noexcept
{
    return *request;
}

RequestFormatError::RequestFormatError(boost::json::object request,
                                       ErrorCode error_response_code,
                                       std::string error_response_message) noexcept
        : Error{error_response_code, std::move(error_response_message)},
          request{std::make_shared<boost::json::object>(std::move(request))}
{}

MissingJsonRpcMemberRequestError::MissingJsonRpcMemberRequestError(boost::json::object request) noexcept
        : RequestFormatError{std::move(request), ErrorCode::InvalidRequest, "missing jsonrpc member"}
{
    assert(!get_request().contains("jsonrpc"));
    std::ostringstream oss;
    oss << "Missing mandatory request member \"jsonrpc\".\nRequest: " << get_request();
    set_what_message(oss.str());
}

InvalidJsonRpcMemberKindError::InvalidJsonRpcMemberKindError(boost::json::object request) noexcept
        : RequestFormatError{std::move(request), ErrorCode::InvalidRequest, "invalid jsonrpc member kind"}
{
    assert(get_request().contains("jsonrpc"));
    assert(get_request().if_contains("jsonrpc")->kind() != expected_kind);
    std::ostringstream oss;
    oss << "Invalid kind for request member \"jsonrpc\".\nExpected: " << expected_kind
        << "\nEncountered: " << get_json_rpc_member_kind() << "\nRequest: " << get_request();
    set_what_message(oss.str());
}

boost::json::kind InvalidJsonRpcMemberKindError::get_json_rpc_member_kind() const noexcept
{
    return get_request().if_contains("jsonrpc")->kind();
}

boost::json::value InvalidJsonRpcMemberKindError::get_error_response_data() const noexcept
{
    return boost::json::object{{"expected_kind", boost::json::to_string(expected_kind)},
                               {"encountered_kind", boost::json::to_string(get_json_rpc_member_kind())}};
}

InvalidJsonRpcMemberValueError::InvalidJsonRpcMemberValueError(boost::json::object request) noexcept
        : RequestFormatError{std::move(request), ErrorCode::InvalidRequest, "invalid jsonrpc member value"}
{
    assert(get_json_rpc_member_value() != expected_value);
    std::ostringstream oss;
    oss << "Invalid value for request member \"jsonrpc\".\nExpected: " << std::quoted(expected_value)
        << "\nEncountered: " << std::quoted(get_json_rpc_member_value()) << "\nRequest: " << get_request();
    set_what_message(oss.str());
}

std::string_view InvalidJsonRpcMemberValueError::get_json_rpc_member_value() const noexcept
{
    assert(get_request().contains("jsonrpc"));
    assert(get_request().if_contains("jsonrpc")->is_string());
    return get_request().if_contains("jsonrpc")->if_string()->c_str();
}

boost::json::value InvalidJsonRpcMemberValueError::get_error_response_data() const noexcept
{
    return boost::json::object{{"expected_value", expected_value}, {"encountered_value", get_json_rpc_member_value()}};
}

MissingMethodMemberRequestError::MissingMethodMemberRequestError(boost::json::object request) noexcept
        : RequestFormatError{std::move(request), ErrorCode::InvalidRequest, "missing method member"}
{
    assert(!request.contains("method"));
    std::ostringstream oss;
    oss << "Missing mandatory request member \"method\".\nRequest: " << get_request();
    set_what_message(oss.str());
}

InvalidMethodMemberKindError::InvalidMethodMemberKindError(boost::json::object request) noexcept
        : RequestFormatError{std::move(request), ErrorCode::InvalidRequest, "invalid method member kind"}
{
    assert(get_request().contains("method"));
    assert(get_request().if_contains("method")->kind() != expected_kind);
    std::ostringstream oss;
    oss << "Invalid kind for request member \"jsonrpc\".\nExpected: " << expected_kind
        << "\nEncountered: " << get_method_member_kind() << "\nRequest: " << get_request();
    set_what_message(oss.str());
}

boost::json::kind InvalidMethodMemberKindError::get_method_member_kind() const noexcept
{
    return get_request().if_contains("method")->kind();
}

boost::json::value InvalidMethodMemberKindError::get_error_response_data() const noexcept
{
    return boost::json::object{{"expected_kind", boost::json::to_string(expected_kind)},
                               {"encountered_kind", boost::json::to_string(get_method_member_kind())}};
}

EmptyMethodMemberError::EmptyMethodMemberError(boost::json::object request) noexcept
        : RequestFormatError{std::move(request), ErrorCode::InvalidRequest, "empty method member"}
{
    assert(get_request().contains("method"));
    assert(get_request().if_contains("method")->is_string());
    assert(get_request().if_contains("method")->as_string().empty());
    std::ostringstream oss;
    oss << "Invalid value for request member \"method\". Method name may not be an empty string.\nRequest : "
        << get_request();
    set_what_message(oss.str());
}

InvalidParamsMemberKindError::InvalidParamsMemberKindError(boost::json::object request) noexcept
        : RequestFormatError{std::move(request), ErrorCode::InvalidRequest, "invalid params member kind"}
{
    assert(get_request().contains("params"));
    assert(!get_params().is_object() && !get_params().is_array());
    std::ostringstream oss;
    oss << "Invalid kind for request member \"params\".\nExpected: object or array\nEncountered: "
        << get_params().kind() << "\nRequest : " << get_request();
    set_what_message(oss.str());
}

boost::json::value& InvalidParamsMemberKindError::get_params() const noexcept
{
    return *get_request().if_contains("params");
}

boost::json::value InvalidParamsMemberKindError::get_error_response_data() const noexcept
{
    return boost::json::object{{"expected_kind",
                                boost::json::array{boost::json::to_string(boost::json::kind::array),
                                                   boost::json::to_string(boost::json::kind::object)}},
                               {"encountered_kind", boost::json::to_string(get_params().kind())}};
}

InvalidIdMemberKindError::InvalidIdMemberKindError(boost::json::object request) noexcept
        : RequestFormatError{std::move(request), ErrorCode::InvalidRequest, "invalid id member kind"}
{
    assert(get_request().contains("id"));
    assert(!get_id().is_number() && !get_id().is_string() && !get_id().is_null());
    std::ostringstream oss;
    oss << "Invalid kind for request member \"id\".\nExpected: number, string or null\nEncountered: " << get_id().kind()
        << "\nRequest : " << get_request();
    set_what_message(oss.str());
}

boost::json::value& InvalidIdMemberKindError::get_id() const noexcept
{
    return *get_request().if_contains("id");
}

boost::json::value InvalidIdMemberKindError::get_error_response_data() const noexcept
{
    return boost::json::object{{"expected_kind", boost::json::array{"number", "string", "null"}},
                               {"encountered_kind", boost::json::to_string(get_id().kind())}};
}

}
