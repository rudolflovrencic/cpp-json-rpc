#include <boost/test/unit_test.hpp>

#include "jsonrpc/server/Request.hpp"

BOOST_AUTO_TEST_SUITE(request_construction);

BOOST_AUTO_TEST_SUITE(faulty_jsonrpc_member);

BOOST_AUTO_TEST_CASE(missing_member_results_in_an_error)
{
    BOOST_CHECK_THROW((jsonrpc::server::Request{{}}), jsonrpc::server::MissingJsonRpcMemberRequestError);
    BOOST_CHECK_THROW((jsonrpc::server::Request{{{"JsonRpc", "2.0"}, {"method", true}}}),
                      jsonrpc::server::MissingJsonRpcMemberRequestError);
    BOOST_CHECK_THROW((jsonrpc::server::Request{{{"JsonRp", "2.0"}}}),
                      jsonrpc::server::MissingJsonRpcMemberRequestError);
}

BOOST_AUTO_TEST_CASE(invalid_kind_results_in_an_error)
{
    BOOST_CHECK_THROW((jsonrpc::server::Request{{{"jsonrpc", false}}}), jsonrpc::server::InvalidJsonRpcMemberKindError);
    BOOST_CHECK_THROW((jsonrpc::server::Request{{{"jsonrpc", 2.0}}}), jsonrpc::server::InvalidJsonRpcMemberKindError);
}

BOOST_AUTO_TEST_CASE(invalid_value_results_in_an_error)
{
    BOOST_CHECK_THROW((jsonrpc::server::Request{{{"jsonrpc", "abc"}}}),
                      jsonrpc::server::InvalidJsonRpcMemberValueError);
    BOOST_CHECK_THROW((jsonrpc::server::Request{{{"jsonrpc", "2.0 "}}}),
                      jsonrpc::server::InvalidJsonRpcMemberValueError);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(faulty_method_member);

BOOST_AUTO_TEST_CASE(missing_member_results_in_an_error)
{
    BOOST_CHECK_THROW((jsonrpc::server::Request{{{"jsonrpc", "2.0"}}}),
                      jsonrpc::server::MissingMethodMemberRequestError);
    BOOST_CHECK_THROW((jsonrpc::server::Request{{{"jsonrpc", "2.0"}, {"Method", "xyz"}}}),
                      jsonrpc::server::MissingMethodMemberRequestError);
}

BOOST_AUTO_TEST_CASE(invalid_kind_results_in_an_error)
{
    BOOST_CHECK_THROW((jsonrpc::server::Request{{{"jsonrpc", "2.0"}, {"method", 42}}}),
                      jsonrpc::server::InvalidMethodMemberKindError);
    BOOST_CHECK_THROW((jsonrpc::server::Request{{{"jsonrpc", "2.0"}, {"method", true}}}),
                      jsonrpc::server::InvalidMethodMemberKindError);
}

BOOST_AUTO_TEST_CASE(empty_value_results_in_an_error)
{
    BOOST_CHECK_THROW((jsonrpc::server::Request{{{"jsonrpc", "2.0"}, {"method", ""}}}),
                      jsonrpc::server::EmptyMethodMemberError);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(faulty_params_member);

BOOST_AUTO_TEST_CASE(invalid_kind_results_in_an_error)
{
    BOOST_CHECK_THROW((jsonrpc::server::Request{{{"jsonrpc", "2.0"}, {"method", "mymethod"}, {"params", true}}}),
                      jsonrpc::server::InvalidParamsMemberKindError);
    BOOST_CHECK_THROW(
        (jsonrpc::server::Request{{{"jsonrpc", "2.0"}, {"method", "mymethod"}, {"params", boost::json::value{}}}}),
        jsonrpc::server::InvalidParamsMemberKindError);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(faulty_id_member);

BOOST_AUTO_TEST_CASE(invalid_kind_results_in_an_error)
{
    BOOST_CHECK_THROW(
        (jsonrpc::server::Request{{{"jsonrpc", "2.0"}, {"method", "mymethod"}, {"id", boost::json::object{}}}}),
        jsonrpc::server::InvalidIdMemberKindError);
    BOOST_CHECK_THROW(
        (jsonrpc::server::Request{{{"jsonrpc", "2.0"}, {"method", "mymethod"}, {"id", boost::json::array{}}}}),
        jsonrpc::server::InvalidIdMemberKindError);
    BOOST_CHECK_THROW((jsonrpc::server::Request{{{"jsonrpc", "2.0"}, {"method", "mymethod"}, {"id", true}}}),
                      jsonrpc::server::InvalidIdMemberKindError);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_CASE(no_params_and_id_members_results_in_valid_request)
{
    const jsonrpc::server::Request request{{{"jsonrpc", "2.0"}, {"method", "mymethod"}}};
    BOOST_CHECK_EQUAL(request.get_method(), "mymethod");
    BOOST_CHECK(!request.get_params());
    BOOST_CHECK(!request.get_id());
}

BOOST_AUTO_TEST_CASE(no_params_member_results_in_valid_request, *boost::unit_test::tolerance(1e-6))
{
    const jsonrpc::server::Request request{{{"jsonrpc", "2.0"}, {"method", "mymethod"}, {"id", 42}}};
    BOOST_CHECK_EQUAL(request.get_method(), "mymethod");
    BOOST_CHECK(!request.get_params());
    const auto* const id = request.get_id();
    BOOST_REQUIRE(id);
    BOOST_CHECK_EQUAL(*id, 42);
}

BOOST_AUTO_TEST_CASE(empty_params_member_results_in_valid_request)
{
    const jsonrpc::server::Request request{
        {{"jsonrpc", "2.0"}, {"method", "mymethod"}, {"params", boost::json::array{}}}};
    BOOST_CHECK_EQUAL(request.get_method(), "mymethod");
    const auto* const params = request.get_params();
    BOOST_REQUIRE(params);
    const auto* const params_as_array = params->if_array();
    BOOST_REQUIRE(params_as_array);
    BOOST_CHECK(params_as_array->empty());
    BOOST_CHECK(!request.get_id());
}

BOOST_AUTO_TEST_CASE(two_position_based_parameters_results_in_valid_request)
{
    const jsonrpc::server::Request request{
        {{"jsonrpc", "2.0"}, {"method", "mymethod"}, {"params", boost::json::array{42, "ab"}}, {"id", "stringid"}}};
    BOOST_CHECK_EQUAL(request.get_method(), "mymethod");
    const auto* const params = request.get_params();
    BOOST_REQUIRE(params);
    const auto* const params_as_array = params->if_array();
    BOOST_REQUIRE(params_as_array);
    BOOST_CHECK_EQUAL(params_as_array->size(), 2);
    BOOST_CHECK_EQUAL((*params_as_array)[0], 42);
    BOOST_CHECK_EQUAL((*params_as_array)[1], "ab");
    const auto* const id = request.get_id();
    BOOST_REQUIRE(id);
    BOOST_CHECK_EQUAL(*id, "stringid");
}

BOOST_AUTO_TEST_CASE(two_name_based_parameters_and_floating_point_id_result_in_valid_request)
{
    const jsonrpc::server::Request request{{{"jsonrpc", "2.0"},
                                            {"method", "mymethod"},
                                            {"params", boost::json::object{{"x", 42}, {"y", 3.14}}},
                                            {"id", 3.18}}};
    BOOST_CHECK_EQUAL(request.get_method(), "mymethod");
    const auto* const params = request.get_params();
    BOOST_REQUIRE(params);
    const auto* const params_as_object = params->if_object();
    BOOST_REQUIRE(params_as_object);
    BOOST_CHECK_EQUAL(params_as_object->at("x"), 42);
    BOOST_CHECK_EQUAL(params_as_object->at("y"), 3.14);
    const auto* const id = request.get_id();
    BOOST_REQUIRE(id);
    const auto* const id_as_double = id->if_double();
    BOOST_REQUIRE(id_as_double);
    BOOST_CHECK_CLOSE(*id_as_double, 3.18, 0.0001);
}

BOOST_AUTO_TEST_CASE(null_id_member_results_in_valid_request)
{
    const jsonrpc::server::Request request{{{"jsonrpc", "2.0"}, {"method", "mymethod"}, {"id", boost::json::value{}}}};
    BOOST_CHECK_EQUAL(request.get_method(), "mymethod");
    BOOST_CHECK(!request.get_params());
    const auto* const id = request.get_id();
    BOOST_REQUIRE(id);
    BOOST_CHECK(id->is_null());
}

BOOST_AUTO_TEST_SUITE_END();
