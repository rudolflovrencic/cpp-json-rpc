project('jsonrpc', 'cpp',
        version: '0.3.0',
        license: 'MIT',
        meson_version: '>= 0.56.2',
        default_options: ['cpp_std=c++17',
                          'warning_level=3',
                          'b_ndebug=if-release'])

boost_json_dep = dependency('boost', version: '>=1.75.0',
                            modules: ['json'])

# Common functionality for the JSON-RPC clients and servers.
common_headers  = files('src/jsonrpc/ErrorCode.hpp')
jsonrpc_include = include_directories('src')

# Functionality for the JSON-RPC servers.
jsonrpc_server_sources = files('src/jsonrpc/server/Error.cpp',
                               'src/jsonrpc/server/Handler.cpp',
                               'src/jsonrpc/server/Request.cpp',
                               'src/jsonrpc/server/Server.cpp')
jsonrpc_server_headers = files('src/jsonrpc/server/Error.hpp',
                               'src/jsonrpc/server/Handler.hpp',
                               'src/jsonrpc/server/Request.hpp',
                               'src/jsonrpc/server/Server.hpp')
libjsonrpc_server = library('libjsonrpc_server', jsonrpc_server_sources,
                            extra_files: [jsonrpc_server_headers, common_headers],
                            include_directories: jsonrpc_include,
                            dependencies: boost_json_dep)
jsonrpc_server_dep = declare_dependency(link_with: libjsonrpc_server,
                                        include_directories: jsonrpc_include,
                                        dependencies: boost_json_dep)

# Functionality for the JSON-RPC clients.
jsonrpc_client_sources = files('src/jsonrpc/client/Error.cpp',
                               'src/jsonrpc/client/Response.cpp')
jsonrpc_client_headers = files('src/jsonrpc/client/Error.hpp',
                               'src/jsonrpc/client/Response.hpp')
libjsonrpc_client = library('libjsonrpc_client', jsonrpc_client_sources,
                            extra_files: [jsonrpc_client_headers, common_headers],
                            include_directories: jsonrpc_include,
                            dependencies: boost_json_dep)
jsonrpc_client_dep = declare_dependency(link_with: libjsonrpc_client,
                                        include_directories: jsonrpc_include,
                                        dependencies: boost_json_dep)

if get_option('test')
    subdir('test')
endif
