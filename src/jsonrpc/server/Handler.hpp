#ifndef JSONRPC_SERVER_HANDLER_HPP
#define JSONRPC_SERVER_HANDLER_HPP

#include "jsonrpc/server/Request.hpp"

#include <vector>
#include <functional>
#include <stdexcept>

namespace jsonrpc::server {

template<typename Ret, typename... Args>
class Handler final {
  private:
    static constexpr std::size_t n_parameters{sizeof...(Args)};

  private:
    std::function<Ret(Args...)> handler;
    std::vector<std::string> parameter_names;

  public:
    Handler(std::function<Ret(Args...)> handler, std::vector<std::string> parameter_names);

    [[nodiscard]] boost::json::value operator()(Request request) const;

  private:
    template<std::size_t... indices>
    boost::json::value invoke_with_indexed_parameters(Request request,
                                                      std::index_sequence<indices...> index_sequence) const;

    template<std::size_t... indices>
    boost::json::value invoke_with_named_parameters(Request request,
                                                    std::index_sequence<indices...> index_sequence) const;
};

template<typename T>
[[nodiscard]] T parse_indexed_parameter(const boost::json::array& params, std::size_t index);

template<typename T>
[[nodiscard]] T parse_named_parameter(const boost::json::object& params,
                                      const std::vector<std::string>& parameter_names,
                                      std::size_t name_index);

class MissingParameterNameError final : public std::runtime_error {
  public:
    explicit MissingParameterNameError(std::string method_name) noexcept;

  private:
    [[nodiscard]] static std::string format_what_message(std::string_view method_name);
};
static_assert(std::is_nothrow_copy_constructible_v<MissingParameterNameError>);

class RequestSemanticError : public Error {
  private:
    std::shared_ptr<Request> request;

  public:
    [[nodiscard]] Request& get_request() const noexcept;

  protected:
    RequestSemanticError(Request request, ErrorCode error_code, std::string error_response_message);
};
static_assert(std::is_nothrow_copy_constructible_v<RequestSemanticError>);

class InvalidNumberOfParametersError final : public RequestSemanticError {
  private:
    std::size_t expected_number_of_parameters;

  public:
    InvalidNumberOfParametersError(Request request, std::size_t expected_number_of_parameters) noexcept;

    [[nodiscard]] std::size_t get_provided_number_of_parameters() const noexcept;
    [[nodiscard]] std::size_t get_expected_number_of_parameters() const noexcept;
    [[nodiscard]] boost::json::value get_error_response_data() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidNumberOfParametersError>);

class IndexedParameterParsingError final : public RequestSemanticError {
  public:
    enum class FaultyIndex : std::size_t {};

  private:
    std::size_t faulty_parameter_index;

  public:
    IndexedParameterParsingError(Request request, FaultyIndex faulty_parameter_index) noexcept;

    [[nodiscard]] std::size_t get_faulty_parameter_index() const noexcept;
    [[nodiscard]] const boost::json::value& get_faulty_parameter() const noexcept;
    [[nodiscard]] boost::json::value get_error_response_data() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<IndexedParameterParsingError>);

class NamedParameterParsingError final : public RequestSemanticError {
  public:
    struct FaultyName final : std::string {};

  private:
    std::shared_ptr<std::string> faulty_parameter_name;

  public:
    NamedParameterParsingError(Request request, FaultyName faulty_parameter_name) noexcept;

    [[nodiscard]] const std::string& get_faulty_parameter_name() const noexcept;
    [[nodiscard]] const boost::json::value& get_faulty_parameter() const noexcept;
    [[nodiscard]] boost::json::value get_error_response_data() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<NamedParameterParsingError>);

class MissingNamedParameterError final : public RequestSemanticError {
  public:
    struct MissingName final : std::string {};

  private:
    std::shared_ptr<std::string> missing_parameter_name;

  public:
    MissingNamedParameterError(Request request, MissingName missing_parameter_name) noexcept;

    [[nodiscard]] const std::string& get_missing_parameter_name() const noexcept;
    [[nodiscard]] boost::json::value get_error_response_data() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<MissingNamedParameterError>);

template<typename Ret, typename... Args>
Handler<Ret, Args...>::Handler(std::function<Ret(Args...)> handler, std::vector<std::string> parameter_names)
        : handler{std::move(handler)}, parameter_names{std::move(parameter_names)}
{}

template<typename Ret, typename... Args>
boost::json::value Handler<Ret, Args...>::operator()(Request request) const
{
    if (request.get_number_of_parameters() != n_parameters) {
        throw InvalidNumberOfParametersError{std::move(request), n_parameters};
    }

    const auto* params        = request.get_params();
    const auto index_sequence = std::index_sequence_for<Args...>();
    return !params || params->is_array() ? invoke_with_indexed_parameters(std::move(request), index_sequence) :
                                           invoke_with_named_parameters(std::move(request), index_sequence);
}

template<typename Ret, typename... Args>
template<std::size_t... indices>
boost::json::value
Handler<Ret, Args...>::invoke_with_indexed_parameters(Request request,
                                                      std::index_sequence<indices...> /*index_sequence*/) const
{
    assert(!request.get_params() || request.get_params()->is_array());

    // Treat no parameters as empty parameters array.
    static const boost::json::array empty_params;
    const auto& params = request.get_params() ? request.get_params()->as_array() : empty_params;

    try {
        if constexpr (std::is_same_v<Ret, void>) {
            handler(parse_indexed_parameter<Args>(params, indices)...);
            return boost::json::value{};
        } else {
            const auto result = handler(parse_indexed_parameter<Args>(params, indices)...);
            return boost::json::value_from(result);
        }
    } catch (IndexedParameterParsingError::FaultyIndex faulty_index) {
        throw IndexedParameterParsingError{std::move(request), faulty_index};
    }
}

template<typename Ret, typename... Args>
template<std::size_t... indices>
boost::json::value
Handler<Ret, Args...>::invoke_with_named_parameters(Request request,
                                                    std::index_sequence<indices...> /*index_sequence*/) const
{
    assert(request.get_params() && request.get_params()->is_object());
    const auto& params = request.get_params()->get_object();

    try {
        if constexpr (std::is_same_v<Ret, void>) {
            handler(parse_named_parameter<Args>(params, parameter_names, indices)...);
            return boost::json::value{};
        } else {
            const auto result = handler(parse_named_parameter<Args>(params, parameter_names, indices)...);
            return boost::json::value_from(result);
        }
    } catch (NamedParameterParsingError::FaultyName faulty_name) {
        throw NamedParameterParsingError{std::move(request), std::move(faulty_name)};
    } catch (MissingNamedParameterError::MissingName missing_name) {
        throw MissingNamedParameterError{std::move(request), std::move(missing_name)};
    }
}

template<typename T>
T parse_indexed_parameter(const boost::json::array& params, std::size_t index)
{
    assert(index < params.size());
    try {
        return boost::json::value_to<T>(params[index]);
    } catch (const std::exception& /*exc*/) {
        throw IndexedParameterParsingError::FaultyIndex{index};
    }
}

template<typename T>
T parse_named_parameter(const boost::json::object& params,
                        const std::vector<std::string>& parameter_names,
                        std::size_t name_index)
{
    assert(name_index < parameter_names.size());
    assert(params.size() == parameter_names.size());
    const auto& parameter_name = parameter_names[name_index];

    const auto it = params.find(parameter_name);
    if (it == params.end()) { throw MissingNamedParameterError::MissingName{parameter_name}; }

    try {
        return boost::json::value_to<T>(it->value());
    } catch (const std::exception& /*exc*/) {
        throw NamedParameterParsingError::FaultyName{parameter_name};
    }
}

}

#endif
