#include "jsonrpc/server/Server.hpp"

#include <sstream>
#include <iomanip>

namespace jsonrpc::server {

namespace {

[[nodiscard]] boost::json::object create_success_response(boost::json::value result, boost::json::value id) noexcept;
[[nodiscard]] boost::json::object create_empty_batch_response() noexcept;
[[nodiscard]] boost::json::object create_failed_json_parse_response() noexcept;
[[nodiscard]] boost::json::object create_invalid_request_kind_response(boost::json::kind request_kind) noexcept;
[[nodiscard]] boost::json::object create_invalid_batch_entry_kind_response(boost::json::kind batch_entry_kind) noexcept;

}

std::optional<boost::json::object> Server::handle_request(boost::json::object request_json) const
{
    auto response_id = request_json.contains("id") ? request_json.at("id") : nullptr;

    try {
        Request request{std::move(request_json)};

        const std::string method{request.get_method()};
        const auto it = handlers.find(method);
        if (it == handlers.end()) { throw MethodNotFoundError{std::move(request), get_registered_method_names()}; }
        const auto& handler = it->second;
        if (!request.get_id()) { // JSON-RPC notification (no response).
            handler(std::move(request));
            return std::nullopt;
        } else { // JSON-RPC request which requires a response.
            auto serialized_result = handler(std::move(request));
            return create_success_response(std::move(serialized_result), std::move(response_id));
        }
    } catch (const Error& error) {
        return error.create_error_response(std::move(response_id));
    }
}

std::optional<boost::json::value> Server::handle_request(boost::json::array batch_request_json) const
{
    if (batch_request_json.empty()) { return create_empty_batch_response(); }

    boost::json::array res;
    res.reserve(batch_request_json.size());
    for (auto& request_json : batch_request_json) {
        if (auto* const request_json_object = request_json.if_object()) {
            if (auto response = handle_request(std::move(*request_json_object))) {
                res.push_back(std::move(*response));
            }
        } else {
            res.push_back(create_invalid_batch_entry_kind_response(request_json.kind()));
        }
    }
    return res.empty() ? std::nullopt : std::optional<boost::json::value>{res};
}

std::optional<std::string> Server::parse_and_handle_request(std::string_view request_string) const
{
    std::error_code error_code;
    auto request = boost::json::parse(request_string, error_code);
    if (!error_code) {
        if (auto* const req_obj = request.if_object()) {
            const auto response = handle_request(std::move(*req_obj));
            return response ? std::optional{boost::json::serialize(*response)} : std::nullopt;
        } else if (auto* const req_arr = request.if_array()) {
            const auto response = handle_request(std::move(*req_arr));
            assert(!response || response->is_array() || response->is_object());
            return response ? std::optional{boost::json::serialize(*response)} : std::nullopt;
        } else {
            return boost::json::serialize(create_invalid_request_kind_response(request.kind()));
        }
    } else {
        return boost::json::serialize(create_failed_json_parse_response());
    }
}

std::unordered_set<std::string> Server::get_registered_method_names() const noexcept
{
    std::unordered_set<std::string> res;
    res.reserve(handlers.size());
    for (const auto& [handler_name, handler] : handlers) { res.insert(handler_name); }
    return res;
}

void Server::validate_parameter_names(std::string_view method,
                                      std::size_t n_parameters,
                                      const std::vector<std::string>& parameter_names)
{
    if (n_parameters != parameter_names.size()) {
        throw IncorrectNumberOfParameterNamesError{std::string{method}, n_parameters, parameter_names.size()};
    }

    const auto it = std::find(parameter_names.begin(), parameter_names.end(), "");
    if (it != parameter_names.end()) { throw EmptyParameterNameError{std::string{method}}; }

    if (!parameter_names.empty()) {
        for (auto it = parameter_names.begin(); it != parameter_names.end() - 1; ++it) {
            const auto found_it = std::find(it + 1, parameter_names.end(), *it);
            if (found_it != parameter_names.end()) { throw DuplicateParameterNameError{std::string{method}, *it}; }
        }
    }
}

MethodRegistrationError::MethodRegistrationError(std::string method_name) noexcept
        : what_message{std::make_shared<std::string>()},
          method_name{std::make_shared<std::string>(std::move(method_name))}
{}

const char* MethodRegistrationError::what() const noexcept
{
    assert(!what_message->empty());
    return what_message->c_str();
}

const std::string& MethodRegistrationError::get_method_name() const noexcept
{
    return *method_name;
}

void MethodRegistrationError::set_what_message(std::string what_message) const noexcept
{
    assert(!what_message.empty());
    *this->what_message = what_message;
}

ReservedMethodNameError::ReservedMethodNameError(std::string method_name) noexcept
        : MethodRegistrationError{std::move(method_name)}
{
    // Provided method name must start with a reserved method name prefix.
    assert(get_method_name().rfind(reserved_method_name_prefix, 0) == 0);

    std::ostringstream oss;
    oss << "Method name " << std::quoted(get_method_name()) << " is reserved.";
    set_what_message(oss.str());
}

IncorrectNumberOfParameterNamesError::IncorrectNumberOfParameterNamesError(std::string method_name,
                                                                           std::size_t n_parameters,
                                                                           std::size_t n_parameter_names) noexcept
        : MethodRegistrationError{std::move(method_name)},
          n_parameters{n_parameters},
          n_parameter_names{n_parameter_names}
{
    assert(get_number_of_parameters() != get_number_of_parameter_names());

    std::ostringstream oss;
    oss << "Method " << std::quoted(get_method_name()) << " expects " << get_number_of_parameters()
        << " parameter names, but " << get_number_of_parameter_names() << " have been provided.";
    set_what_message(oss.str());
}

std::size_t IncorrectNumberOfParameterNamesError::get_number_of_parameters() const noexcept
{
    return n_parameters;
}

std::size_t IncorrectNumberOfParameterNamesError::get_number_of_parameter_names() const noexcept
{
    return n_parameter_names;
}

EmptyParameterNameError::EmptyParameterNameError(std::string method) noexcept
        : MethodRegistrationError{std::move(method)}
{
    std::ostringstream oss;
    oss << "Method " << std::quoted(get_method_name()) << " contains an empty string as parameter name.";
    set_what_message(oss.str());
}

DuplicateParameterNameError::DuplicateParameterNameError(std::string method_name,
                                                         std::string duplicate_parameter_name) noexcept
        : MethodRegistrationError{std::move(method_name)},
          duplicate_parameter_name{std::make_shared<std::string>(std::move(duplicate_parameter_name))}
{
    std::ostringstream oss;
    oss << "Method " << std::quoted(get_method_name()) << " has multiple parameter named "
        << std::quoted(get_duplicate_parameter_name()) << '.';
    set_what_message(oss.str());
}

const std::string& DuplicateParameterNameError::get_duplicate_parameter_name() const noexcept
{
    return *duplicate_parameter_name;
}

MethodNotFoundError::MethodNotFoundError(Request request,
                                         std::unordered_set<std::string> registered_method_names) noexcept
        : RequestSemanticError{std::move(request), ErrorCode::MethodNotFound, "method not found"},
          registered_method_names{std::make_shared<std::unordered_set<std::string>>(std::move(registered_method_names))}
{
    assert(get_registered_method_names().count(std::string{get_method()}) == 0);
    std::ostringstream oss;
    oss << "Method " << std::quoted(get_method()) << " not found.\nRegistered method names: {";
    const auto& rm = get_registered_method_names();
    if (!rm.empty()) {
        oss << std::quoted(*rm.begin());
        for (auto it = std::next(rm.begin()); it != rm.end(); ++it) { oss << ", " << std::quoted(*it) << ' '; }
    }
    oss << "\nRequest: " << get_request().get_json();
    set_what_message(oss.str());
}

std::string_view MethodNotFoundError::get_method() const noexcept
{
    return get_request().get_method();
}

std::unordered_set<std::string>& MethodNotFoundError::get_registered_method_names() const noexcept
{
    return *registered_method_names;
}

boost::json::value MethodNotFoundError::get_error_response_data() const noexcept
{
    boost::json::array registered_methods;
    for (const auto& method : get_registered_method_names()) {
        registered_methods.push_back(boost::json::string{method});
    }

    return boost::json::object{{"called_method", get_method()}, {"registered_methods", std::move(registered_methods)}};
}

namespace {

boost::json::object create_success_response(boost::json::value result, boost::json::value id) noexcept
{
    assert(id.is_string() || id.is_number() || id.is_null());
    return boost::json::object{{"jsonrpc", "2.0"}, {"result", std::move(result)}, {"id", std::move(id)}};
}

boost::json::object create_empty_batch_response() noexcept
{
    return create_error_response(ErrorCode::InvalidRequest, "empty batch error");
}

boost::json::object create_failed_json_parse_response() noexcept
{
    return create_error_response(ErrorCode::ParseError, "json parse error");
}

boost::json::object create_invalid_request_kind_response(boost::json::kind request_kind) noexcept
{
    static constexpr std::array expected_kinds{boost::json::kind::object, boost::json::kind::array};
    assert(std::find(expected_kinds.begin(), expected_kinds.end(), request_kind) == expected_kinds.end());

    return create_error_response(ErrorCode::InvalidRequest,
                                 "invalid request kind",
                                 boost::json::object{{"encountered_kind", boost::json::to_string(request_kind)},
                                                     {"expected_kinds",
                                                      boost::json::array{boost::json::to_string(expected_kinds[0]),
                                                                         boost::json::to_string(expected_kinds[1])}}});
}

boost::json::object create_invalid_batch_entry_kind_response(boost::json::kind batch_entry_kind) noexcept
{
    static constexpr auto expected_kind = boost::json::kind::object;
    assert(batch_entry_kind != expected_kind);

    return create_error_response(ErrorCode::InvalidRequest,
                                 "invalid batch entry kind",
                                 boost::json::object{{"encountered_kind", boost::json::to_string(batch_entry_kind)},
                                                     {"expected_kind", boost::json::to_string(expected_kind)}});
}

}

}
