#include <boost/test/unit_test.hpp>

#include "jsonrpc/server/Server.hpp"

namespace {

struct Point final {
    int x;
    int y;
};

[[nodiscard]] [[maybe_unused]] Point tag_invoke(const boost::json::value_to_tag<Point>& /*tag*/,
                                                const boost::json::value& value)
{
    return Point{boost::json::value_to<int>(value.at("x")), boost::json::value_to<int>(value.at("y"))};
}

[[maybe_unused]] void
tag_invoke(const boost::json::value_from_tag& /*tag*/, boost::json::value& value, const Point& point)
{
    value = boost::json::object{{"x", boost::json::value_from(point.x)}, {"y", boost::json::value_from(point.y)}};
}

}

BOOST_AUTO_TEST_SUITE(method_registration);

BOOST_AUTO_TEST_CASE(reserved_method_name_results_in_an_error)
{
    jsonrpc::server::Server server;
    auto method_name = std::string{jsonrpc::server::reserved_method_name_prefix} + "my_method";
    BOOST_CHECK_THROW(server.add(std::move(method_name), []() noexcept -> void {}, {}),
                      jsonrpc::server::ReservedMethodNameError);
}

BOOST_AUTO_TEST_CASE(incorrect_nubmer_of_parameter_names_results_in_an_error)
{
    jsonrpc::server::Server server;
    BOOST_CHECK_EXCEPTION(
        server.add("test_method", [](int x) -> std::string { return std::to_string(x); }, {"x", "extra_name"}),
        jsonrpc::server::IncorrectNumberOfParameterNamesError,
        [](const jsonrpc::server::IncorrectNumberOfParameterNamesError& err) noexcept -> bool {
            return err.get_method_name() == "test_method" && err.get_number_of_parameters() == 1 &&
                   err.get_number_of_parameter_names() == 2;
        });
}

BOOST_AUTO_TEST_CASE(empty_parameter_name_results_in_an_error)
{
    jsonrpc::server::Server server;
    BOOST_CHECK_THROW(server.add("test_method", [](int x) -> std::string { return std::to_string(x); }, {""}),
                      jsonrpc::server::EmptyParameterNameError);
}

BOOST_AUTO_TEST_CASE(duplicate_parameter_name_results_in_an_error)
{
    jsonrpc::server::Server server;
    BOOST_CHECK_EXCEPTION(server.add("to_string_concat",
                                     [](int x, int y, int z) noexcept -> std::string {
                                         return std::to_string(x) + ',' + std::to_string(y) + ',' + std::to_string(z);
                                     },
                                     {"x", "y", "y"}),
                          jsonrpc::server::DuplicateParameterNameError,
                          [](const jsonrpc::server::DuplicateParameterNameError& err) -> bool {
                              return err.get_method_name() == "to_string_concat" &&
                                     err.get_duplicate_parameter_name() == "y";
                          });
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(request_handling);

BOOST_AUTO_TEST_CASE(non_valid_json_string_results_in_an_error)
{
    jsonrpc::server::Server server;
    const auto response = server.parse_and_handle_request(R"(
        {
            "jsonrpc": "2.0", /* unsupported comment */
            "method": "sub",
            "params": [42, 3.14]
            "id": [42, 3.14]
        }
    )");
    BOOST_REQUIRE(response.has_value());
    const auto response_val = boost::json::parse(*response);
    BOOST_REQUIRE(response_val.is_object());
    const auto& response_obj = response_val.get_object();
    BOOST_CHECK(!response_obj.contains("result"));
    const auto* const error = response_obj.if_contains("error");
    BOOST_REQUIRE(((error && error->is_object())));
    BOOST_CHECK_EQUAL(error->at("code").as_int64(), static_cast<int>(jsonrpc::ErrorCode::ParseError));
    BOOST_CHECK_EQUAL(error->at("message").as_string(), "json parse error");
    BOOST_CHECK(response_obj.at("id").is_null());
}

BOOST_AUTO_TEST_CASE(invalid_request_kind_results_in_an_error)
{
    jsonrpc::server::Server server;
    const auto response = server.parse_and_handle_request(R"(
            "json_string"
    )");
    BOOST_REQUIRE(response.has_value());
    const auto response_val = boost::json::parse(*response);
    BOOST_REQUIRE(response_val.is_object());
    const auto& response_obj = response_val.get_object();
    BOOST_CHECK(!response_obj.contains("result"));
    const auto* const error = response_obj.if_contains("error");
    BOOST_REQUIRE((error && error->is_object()));
    BOOST_CHECK_EQUAL(error->at("code").as_int64(), static_cast<int>(jsonrpc::ErrorCode::InvalidRequest));
    BOOST_CHECK_EQUAL(error->at("message").as_string(), "invalid request kind");
    const auto* const data = error->get_object().if_contains("data");
    BOOST_REQUIRE((data && data->is_object()));
    BOOST_CHECK_EQUAL(error->at("data").as_object().at("encountered_kind"),
                      boost::json::to_string(boost::json::kind::string));
    BOOST_CHECK(response_obj.at("id").is_null());
}

BOOST_AUTO_TEST_CASE(no_registered_methods_results_in_an_error)
{
    jsonrpc::server::Server server;
    const auto response = server.handle_request(boost::json::object{
        {"jsonrpc", "2.0"}, {"method", "mymethod"}, {"params", boost::json::array{42, "ab"}}, {"id", 42}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("result"));
    const auto* const error = response->if_contains("error");
    BOOST_REQUIRE((error && error->is_object()));
    BOOST_CHECK_EQUAL(error->at("code").as_int64(), static_cast<int>(jsonrpc::ErrorCode::MethodNotFound));
    BOOST_CHECK_EQUAL(error->at("message").as_string(), "method not found");
    BOOST_CHECK_EQUAL(response->at("id"), 42);
}

BOOST_AUTO_TEST_CASE(method_name_case_missmatch_results_in_an_error)
{
    jsonrpc::server::Server server;
    server.add("method1", []() noexcept -> void {}, {});
    server.add("method2", [](int /*n*/) noexcept -> void {}, {"n"});
    server.add("method4", []() noexcept -> int { return 42; }, {});
    const auto response = server.handle_request(boost::json::object{
        {"jsonrpc", "2.0"}, {"method", "Method1"}, {"params", boost::json::array{42, "ab"}}, {"id", "stringid"}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("result"));
    const auto* const error = response->if_contains("error");
    BOOST_REQUIRE((error && error->is_object()));
    BOOST_CHECK_EQUAL(error->at("code").as_int64(), static_cast<int>(jsonrpc::ErrorCode::MethodNotFound));
    BOOST_CHECK_EQUAL(error->at("message").as_string(), "method not found");
    BOOST_CHECK_EQUAL(response->at("id"), "stringid");
}

BOOST_AUTO_TEST_SUITE(faulty_position_based_parameters);

BOOST_AUTO_TEST_CASE(missing_parameter_in_the_request_results_in_an_error)
{
    jsonrpc::server::Server server;
    server.add("method2", [](int /*n*/) -> void {}, {"n"});
    const auto response =
        server.handle_request(boost::json::object{{"jsonrpc", "2.0"}, {"method", "method2"}, {"id", 42}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("result"));
    const auto* const error = response->if_contains("error");
    BOOST_REQUIRE((error && error->is_object()));
    BOOST_CHECK_EQUAL(error->at("code").as_int64(), static_cast<int>(jsonrpc::ErrorCode::InvalidParams));
    BOOST_CHECK_EQUAL(error->at("message").as_string(), "invalid number of parameters");
    BOOST_CHECK_EQUAL(response->at("id"), 42);
    const auto* const data = error->get_object().if_contains("data");
    BOOST_REQUIRE(data);
    BOOST_CHECK_EQUAL(data->at("provided_number_of_parameters"), 0);
    BOOST_CHECK_EQUAL(data->at("expected_number_of_parameters"), 1);
}

BOOST_AUTO_TEST_CASE(too_many_parameters_in_the_request_results_in_an_error)
{
    jsonrpc::server::Server server;
    server.add("method1", []() {}, {});
    const auto response = server.handle_request(
        boost::json::object{{"jsonrpc", "2.0"}, {"method", "method1"}, {"params", boost::json::array{42, "ab"}}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("result"));
    const auto* const error = response->if_contains("error");
    BOOST_REQUIRE((error && error->is_object()));
    BOOST_CHECK_EQUAL(error->at("code").as_int64(), static_cast<int>(jsonrpc::ErrorCode::InvalidParams));
    BOOST_CHECK_EQUAL(error->at("message").as_string(), "invalid number of parameters");
    BOOST_CHECK(response->at("id").is_null());
    const auto* const data = error->get_object().if_contains("data");
    BOOST_REQUIRE(data);
    BOOST_CHECK_EQUAL(data->at("provided_number_of_parameters"), 2);
    BOOST_CHECK_EQUAL(data->at("expected_number_of_parameters"), 0);
}

BOOST_AUTO_TEST_CASE(double_value_in_place_of_a_string_argument_results_in_an_error)
{
    jsonrpc::server::Server server;
    server.add("method1", [](int /*n*/, std::string /*name*/) {}, {"n", "name"});

    const auto response = server.handle_request(boost::json::object{
        {"jsonrpc", "2.0"}, {"method", "method1"}, {"params", boost::json::array{42, 3.14}}, {"id", 23}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("result"));
    const auto* const error = response->if_contains("error");
    BOOST_REQUIRE((error && error->is_object()));
    BOOST_CHECK_EQUAL(error->at("code").as_int64(), static_cast<int>(jsonrpc::ErrorCode::InvalidParams));
    BOOST_CHECK_EQUAL(error->at("message").as_string(), "parameter parsing error");
    BOOST_CHECK_EQUAL(response->at("id"), 23);
    const auto* const data = error->get_object().if_contains("data");
    BOOST_REQUIRE(data);
    BOOST_CHECK_EQUAL(data->at("faulty_parameter_index"), 1);
}

BOOST_AUTO_TEST_CASE(double_value_in_place_of_an_int_argument_in_user_provided_type_results_in_an_error)
{
    jsonrpc::server::Server server;
    server.add("method2", [](int /*n*/, double /*v*/, Point /*point*/) {}, {"n", "v", "point"});

    const auto response = server.handle_request(
        boost::json::object{{"jsonrpc", "2.0"},
                            {"method", "method2"},
                            {"params", boost::json::array{42, 3.14, boost::json::object{{"x", 1.28}, {"y", "2.32"}}}},
                            {"id", 23}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("result"));
    const auto* const error = response->if_contains("error");
    BOOST_REQUIRE((error && error->is_object()));
    BOOST_CHECK_EQUAL(error->at("code").as_int64(), static_cast<int>(jsonrpc::ErrorCode::InvalidParams));
    BOOST_CHECK_EQUAL(error->at("message").as_string(), "parameter parsing error");
    BOOST_CHECK_EQUAL(response->at("id"), 23);
    const auto* const data = error->as_object().if_contains("data");
    BOOST_REQUIRE((data && data->is_object()));
    BOOST_CHECK_EQUAL(data->at("faulty_parameter_index"), 2);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(faulty_name_based_parameters);

BOOST_AUTO_TEST_CASE(missing_parameter_in_the_request_results_in_an_error)
{
    jsonrpc::server::Server server;
    server.add("increment", [](int x) noexcept { return x + 1; }, {"x"});
    const auto response = server.handle_request(boost::json::object{
        {"jsonrpc", "2.0"}, {"method", "increment"}, {"params", boost::json::object{{"y", 42}}}, {"id", 18}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("result"));
    const auto* const error = response->if_contains("error");
    BOOST_REQUIRE((error && error->is_object()));
    BOOST_CHECK_EQUAL(error->at("code").as_int64(), static_cast<int>(jsonrpc::ErrorCode::InvalidParams));
    BOOST_CHECK_EQUAL(error->at("message").as_string(), "missing named parameter");
    BOOST_CHECK_EQUAL(response->at("id").as_int64(), 18);
    const auto* const data = error->get_object().if_contains("data");
    BOOST_REQUIRE(data);
    BOOST_CHECK_EQUAL(data->at("missing_parameter_name"), "x");
}

BOOST_AUTO_TEST_CASE(too_many_parameters_in_the_request_results_in_an_error)
{
    jsonrpc::server::Server server;
    server.add("increment", [](int x) noexcept { return x + 1; }, {"x"});
    const auto response = server.handle_request(
        boost::json::object{{"jsonrpc", "2.0"},
                            {"method", "increment"},
                            {"params", boost::json::object{{"z", "0x8"}, {"y", 3.14}, {"x", 42}}}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("result"));
    const auto* const error = response->if_contains("error");
    BOOST_REQUIRE((error && error->is_object()));
    BOOST_CHECK_EQUAL(error->at("code").as_int64(), static_cast<int>(jsonrpc::ErrorCode::InvalidParams));
    BOOST_CHECK_EQUAL(error->at("message").as_string(), "invalid number of parameters");
    BOOST_CHECK(response->at("id").is_null());
    const auto* const data = error->get_object().if_contains("data");
    BOOST_REQUIRE(data);
    BOOST_CHECK_EQUAL(data->at("provided_number_of_parameters"), 3);
    BOOST_CHECK_EQUAL(data->at("expected_number_of_parameters"), 1);
}

BOOST_AUTO_TEST_CASE(double_value_in_place_of_a_string_argument_results_in_an_error)
{
    jsonrpc::server::Server server;
    server.add("method1", [](int /*n*/, std::string /*name*/) {}, {"n", "name"});

    const auto response =
        server.handle_request(boost::json::object{{"jsonrpc", "2.0"},
                                                  {"method", "method1"},
                                                  {"params", boost::json::object{{"n", 42}, {"name", 3.14}}},
                                                  {"id", 23}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("result"));
    const auto* const error = response->if_contains("error");
    BOOST_REQUIRE((error && error->is_object()));
    BOOST_CHECK_EQUAL(error->at("code").as_int64(), static_cast<int>(jsonrpc::ErrorCode::InvalidParams));
    BOOST_CHECK_EQUAL(error->at("message").as_string(), "parameter parsing error");
    BOOST_CHECK_EQUAL(response->at("id"), 23);
    const auto* const data = error->get_object().if_contains("data");
    BOOST_REQUIRE(data);
    BOOST_CHECK_EQUAL(data->at("faulty_parameter_name"), "name");
}

BOOST_AUTO_TEST_CASE(double_value_in_place_of_an_int_argument_in_user_provided_type_results_in_an_error)
{
    jsonrpc::server::Server server;
    server.add("method2", [](int /*n*/, double /*v*/, Point /*point*/) {}, {"n", "v", "point"});

    const auto response = server.handle_request(boost::json::object{
        {"jsonrpc", "2.0"},
        {"method", "method2"},
        {"params",
         boost::json::object{{"point", boost::json::object{{"x", 1.28}, {"y", "2.32"}}}, {"v", 3.14}, {"n", 24}}},
        {"id", 23}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("result"));
    const auto* const error = response->if_contains("error");
    BOOST_REQUIRE((error && error->is_object()));
    BOOST_CHECK_EQUAL(error->at("code").as_int64(), static_cast<int>(jsonrpc::ErrorCode::InvalidParams));
    BOOST_CHECK_EQUAL(error->at("message").as_string(), "parameter parsing error");
    BOOST_CHECK_EQUAL(response->at("id"), 23);
    const auto* const data = error->as_object().if_contains("data");
    BOOST_REQUIRE((data && data->is_object()));
    BOOST_CHECK_EQUAL(data->at("faulty_parameter_name"), "point");
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(valid_request);

struct Fixture {
    Fixture() noexcept
    {
        server.add("string_converter", [](int n) noexcept { return std::to_string(n); }, {"n"});
        server.add("add", [](int a, double b) noexcept { return a + b; }, {"a", "b"});
        server.add("swap", [](Point p) noexcept { return Point{p.y, p.x}; }, {"point"});
        server.add("empty_func", []() noexcept {}, {});
        server.add("void_func", [](int /*x*/) noexcept {}, {"x"});
    }

    jsonrpc::server::Server server;
};

BOOST_FIXTURE_TEST_CASE(calling_a_function_that_takes_no_parameters_results_in_success, Fixture)
{
    const auto response =
        server.handle_request(boost::json::object{{"jsonrpc", "2.0"}, {"method", "empty_func"}, {"id", 77}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("error"));
    const auto* const result = response->if_contains("result");
    BOOST_REQUIRE(result);
    BOOST_CHECK_EQUAL(*result, boost::json::value{});
    BOOST_CHECK_EQUAL(response->at("id"), 77);
}

BOOST_FIXTURE_TEST_CASE(calling_a_void_function_results_in_success, Fixture)
{
    const auto response = server.handle_request(boost::json::object{
        {"jsonrpc", "2.0"}, {"method", "void_func"}, {"params", boost::json::object{{"x", 38}}}, {"id", 1239}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("error"));
    const auto* const result = response->if_contains("result");
    BOOST_REQUIRE(result);
    BOOST_CHECK_EQUAL(*result, boost::json::value{});
    BOOST_CHECK_EQUAL(response->at("id"), 1239);
}

BOOST_AUTO_TEST_SUITE(position_based_parameters);

BOOST_FIXTURE_TEST_CASE(calling_a_function_that_converts_int_to_string_results_in_success, Fixture)
{
    const auto response = server.handle_request(boost::json::object{{"jsonrpc", "2.0"},
                                                                    {"method", "string_converter"},
                                                                    {"params", boost::json::array{42}},
                                                                    {"id", boost::json::value{}}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("error"));
    const auto* const result = response->if_contains("result");
    BOOST_REQUIRE(result);
    BOOST_CHECK_EQUAL(*result, "42");
    BOOST_CHECK(response->at("id").is_null());
}

BOOST_FIXTURE_TEST_CASE(calling_a_function_that_adds_an_int_and_a_double_results_in_success, Fixture)
{
    const auto response = server.handle_request(boost::json::object{
        {"jsonrpc", "2.0"}, {"method", "add"}, {"params", boost::json::array{42, 3.14}}, {"id", 28}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("error"));
    const auto* const result = response->if_contains("result");
    BOOST_REQUIRE(result);
    BOOST_CHECK_CLOSE(result->as_double(), 45.14, 1e-6);
    BOOST_CHECK_EQUAL(response->at("id"), 28);
}

BOOST_FIXTURE_TEST_CASE(calling_a_function_that_swaps_two_elements_of_a_user_provided_type_results_in_success, Fixture)
{
    const auto response = server.handle_request(
        boost::json::object{{"jsonrpc", "2.0"},
                            {"method", "swap"},
                            {"params", boost::json::array{boost::json::object{{"x", 1}, {"y", 2}}}},
                            {"id", "swapid"}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("error"));
    const auto* const result = response->if_contains("result");
    BOOST_REQUIRE(result);
    const auto point = boost::json::value_to<Point>(*result);
    BOOST_CHECK_EQUAL(point.x, 2);
    BOOST_CHECK_EQUAL(point.y, 1);
    BOOST_CHECK_EQUAL(response->at("id"), "swapid");
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(name_based_parameters);

BOOST_FIXTURE_TEST_CASE(calling_a_function_that_converts_int_to_string_results_in_success, Fixture)
{
    const auto response = server.handle_request(boost::json::object{{"jsonrpc", "2.0"},
                                                                    {"method", "string_converter"},
                                                                    {"params", boost::json::object{{"n", 42}}},
                                                                    {"id", boost::json::value{}}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("error"));
    const auto* const result = response->if_contains("result");
    BOOST_REQUIRE(result);
    BOOST_CHECK_EQUAL(*result, "42");
    BOOST_CHECK(response->at("id").is_null());
}

BOOST_FIXTURE_TEST_CASE(calling_a_function_that_adds_an_int_and_a_double_results_in_success, Fixture)
{
    const auto response = server.handle_request(boost::json::object{
        {"jsonrpc", "2.0"}, {"method", "add"}, {"params", boost::json::object{{"a", 42}, {"b", 3.14}}}, {"id", 28}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("error"));
    const auto* const result = response->if_contains("result");
    BOOST_REQUIRE(result);
    BOOST_CHECK_CLOSE(result->as_double(), 45.14, 1e-6);
    BOOST_CHECK_EQUAL(response->at("id"), 28);
}

BOOST_FIXTURE_TEST_CASE(calling_a_function_that_swaps_two_elements_of_a_user_provided_type_results_in_success, Fixture)
{
    const auto response = server.handle_request(
        boost::json::object{{"jsonrpc", "2.0"},
                            {"method", "swap"},
                            {"params", boost::json::object{{"point", boost::json::object{{"x", 1}, {"y", 2}}}}},
                            {"id", "swapid"}});
    BOOST_REQUIRE(response.has_value());
    BOOST_CHECK(!response->contains("error"));
    const auto* const result = response->if_contains("result");
    BOOST_REQUIRE(result);
    const auto point = boost::json::value_to<Point>(*result);
    BOOST_CHECK_EQUAL(point.x, 2);
    BOOST_CHECK_EQUAL(point.y, 1);
    BOOST_CHECK_EQUAL(response->at("id"), "swapid");
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(valid_notification);

BOOST_AUTO_TEST_SUITE(position_based_parameters);

BOOST_AUTO_TEST_CASE(calling_a_function_with_return_value_results_in_no_response)
{
    jsonrpc::server::Server server;
    server.add("swap", [](Point p) noexcept { return Point{p.y, p.x}; }, {"p"});
    const auto response = server.handle_request(
        boost::json::object{{"jsonrpc", "2.0"},
                            {"method", "swap"},
                            {"params", boost::json::array{boost::json::object{{"x", 1}, {"y", 2}}}}});
    BOOST_CHECK(!response.has_value());
}

BOOST_AUTO_TEST_CASE(calling_a_function_without_return_value_results_in_no_response)
{
    jsonrpc::server::Server server;
    server.add("empty_func", []() noexcept {}, {});
    const auto response = server.handle_request(boost::json::object{{"jsonrpc", "2.0"}, {"method", "empty_func"}});
    BOOST_CHECK(!response.has_value());
}

BOOST_AUTO_TEST_CASE(calling_a_function_with_json_as_a_string_results_in_no_response)
{
    jsonrpc::server::Server server;
    server.add("sub", [](int a, double b) { return a - b; }, {"a", "b"});
    const auto response = server.parse_and_handle_request(R"(
        {
            "jsonrpc": "2.0",
            "method": "sub",
            "params": [42, 3.14]
        }
    )");
    BOOST_CHECK(!response.has_value());
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(batch_request);

BOOST_AUTO_TEST_CASE(empty_batch_results_in_an_error)
{
    jsonrpc::server::Server server;
    const auto response = server.handle_request(boost::json::array{});
    BOOST_REQUIRE(response.has_value());
    BOOST_REQUIRE(response->is_object());
    const auto& response_obj = response->get_object();
    BOOST_CHECK(!response_obj.contains("result"));
    const auto* const error = response_obj.if_contains("error");
    BOOST_REQUIRE(((error && error->is_object())));
    BOOST_CHECK_EQUAL(error->at("code").as_int64(), static_cast<int>(jsonrpc::ErrorCode::InvalidRequest));
    BOOST_CHECK_EQUAL(error->at("message").as_string(), "empty batch error");
    BOOST_CHECK(response_obj.at("id").is_null());
}

BOOST_AUTO_TEST_CASE(invalid_batch_entry_kind_results_in_an_error)
{
    jsonrpc::server::Server server;
    server.add("f", [](int a, double b) { return a - b; }, {"a", "b"});
    server.add("g", [](int x, int y) { return x + y; }, {"x", "y"});
    server.add("empty_method", []() {}, {});
    const auto response = server.parse_and_handle_request(R"(
        [
            { "jsonrpc": "2.0", "method": "f", "params": [42, 3.14], "id": 1 },
            "json_string",
            { "jsonrpc": "2.0", "method": "g", "params": [18, 13]  , "id": 2}
        ]
    )");
    BOOST_REQUIRE(response.has_value());
    const auto response_val = boost::json::parse(*response);
    BOOST_REQUIRE(response_val.is_array());
    const auto& response_arr = response_val.get_array();
    BOOST_REQUIRE_EQUAL(response_arr.size(), 3);
    {
        const auto* response_obj = response_arr[0].if_object();
        BOOST_REQUIRE(response_obj);
        BOOST_CHECK(response_obj->contains("result"));
    }
    {
        const auto* response_obj = response_arr[1].if_object();
        const auto* const error  = response_obj->if_contains("error");
        BOOST_REQUIRE((error && error->is_object()));
        BOOST_CHECK_EQUAL(error->at("code").as_int64(), static_cast<int>(jsonrpc::ErrorCode::InvalidRequest));
        BOOST_CHECK_EQUAL(error->at("message").as_string(), "invalid batch entry kind");
        const auto* const data = error->get_object().if_contains("data");
        BOOST_REQUIRE((data && data->is_object()));
        BOOST_CHECK(error->at("data").as_object().at("encountered_kind") ==
                    boost::json::to_string(boost::json::kind::string));
        BOOST_CHECK(response_obj->at("id").is_null());
    }
    {
        const auto* response_obj = response_arr[2].if_object();
        BOOST_CHECK(response_obj);
        BOOST_CHECK(response_obj->contains("result"));
    }
}

BOOST_AUTO_TEST_CASE(handling_a_notification_only_batch_results_in_no_response)
{
    jsonrpc::server::Server server;
    server.add("sub", [](int a, double b) { return a - b; }, {"a", "b"});
    server.add("add", [](int x, int y) { return x + y; }, {"x", "y"});
    server.add("empty_method", []() {}, {});
    const auto response = server.parse_and_handle_request(R"(
        [
            { "jsonrpc": "2.0", "method": "sub", "params": [42, 3.14] },
            { "jsonrpc": "2.0", "method": "sub", "params": [18, 13.8] },
            { "jsonrpc": "2.0", "method": "add", "params": [18, 13]   },
            { "jsonrpc": "2.0", "method": "empty_method"              }
        ]
    )");
    BOOST_CHECK(!response.has_value());
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE_END();
