#include "jsonrpc/server/Handler.hpp"

#include <iomanip>
#include <sstream>

namespace jsonrpc::server {

Request& RequestSemanticError::get_request() const noexcept
{
    assert(request);
    return *request;
}

RequestSemanticError::RequestSemanticError(Request request, ErrorCode error_code, std::string error_response_message)
        : Error{error_code, std::move(error_response_message)}, request{std::make_shared<Request>(std::move(request))}
{}

InvalidNumberOfParametersError::InvalidNumberOfParametersError(Request request,
                                                               std::size_t expected_number_of_parameters) noexcept
        : RequestSemanticError{std::move(request), ErrorCode::InvalidParams, "invalid number of parameters"},
          expected_number_of_parameters{expected_number_of_parameters}
{
    assert(get_provided_number_of_parameters() != get_expected_number_of_parameters());
    std::ostringstream oss;
    oss << "Invalid number of parameters error. Expected " << get_expected_number_of_parameters() << " parameters, but "
        << get_provided_number_of_parameters() << " were provided.\nRequest: " << get_request().get_json();
    set_what_message(oss.str());
}

std::size_t InvalidNumberOfParametersError::get_provided_number_of_parameters() const noexcept
{
    return get_request().get_number_of_parameters();
}

std::size_t InvalidNumberOfParametersError::get_expected_number_of_parameters() const noexcept
{
    return expected_number_of_parameters;
}

boost::json::value InvalidNumberOfParametersError::get_error_response_data() const noexcept
{
    return boost::json::object{{"provided_number_of_parameters", get_provided_number_of_parameters()},
                               {"expected_number_of_parameters", get_expected_number_of_parameters()}};
}

IndexedParameterParsingError::IndexedParameterParsingError(Request request, FaultyIndex faulty_parameter_index) noexcept
        : RequestSemanticError{std::move(request), ErrorCode::InvalidParams, "parameter parsing error"},
          faulty_parameter_index{static_cast<std::size_t>(faulty_parameter_index)}
{
    assert(get_request().get_params());
    assert(get_request().get_params()->is_array());
    assert(get_request().get_number_of_parameters() > get_faulty_parameter_index());
    std::ostringstream oss;
    oss << "Failed to parse paramter and index " << get_faulty_parameter_index()
        << "\n.Request: " << get_request().get_json();
    set_what_message(oss.str());
}

std::size_t IndexedParameterParsingError::get_faulty_parameter_index() const noexcept
{
    return faulty_parameter_index;
}

const boost::json::value& IndexedParameterParsingError::get_faulty_parameter() const noexcept
{
    const auto index = get_faulty_parameter_index();
    return get_request().get_params()->get_array()[index];
}

boost::json::value IndexedParameterParsingError::get_error_response_data() const noexcept
{
    return boost::json::object{{"faulty_parameter_index", get_faulty_parameter_index()}};
}

NamedParameterParsingError::NamedParameterParsingError(Request request, FaultyName faulty_parameter_name) noexcept
        : RequestSemanticError{std::move(request), ErrorCode::InvalidParams, "parameter parsing error"},
          faulty_parameter_name{std::make_shared<std::string>(std::move(faulty_parameter_name))}
{
    assert(get_request().get_params());
    assert(get_request().get_params()->is_object());
    assert(get_request().get_params()->get_object().contains(get_faulty_parameter_name()));
    std::ostringstream oss;
    oss << "Failed to parse paramter named " << std::quoted(get_faulty_parameter_name())
        << "\n.Request: " << get_request().get_json();
    set_what_message(oss.str());
}

const std::string& NamedParameterParsingError::get_faulty_parameter_name() const noexcept
{
    return *faulty_parameter_name;
}

const boost::json::value& NamedParameterParsingError::get_faulty_parameter() const noexcept
{
    return get_request().get_params()->get_object().at(get_faulty_parameter_name());
}

boost::json::value NamedParameterParsingError::get_error_response_data() const noexcept
{
    return boost::json::object{{"faulty_parameter_name", get_faulty_parameter_name()}};
}

MissingNamedParameterError::MissingNamedParameterError(Request request, MissingName missing_parameter_name) noexcept
        : RequestSemanticError{std::move(request), ErrorCode::InvalidParams, "missing named parameter"},
          missing_parameter_name{std::make_shared<std::string>(std::move(missing_parameter_name))}
{
    assert(get_request().get_params());
    assert(get_request().get_params()->is_object());
    assert(!get_request().get_params()->get_object().contains(get_missing_parameter_name()));
    std::ostringstream oss;
    oss << "Missing parameter " << std::quoted(get_missing_parameter_name())
        << "\n.Request: " << get_request().get_json();
    set_what_message(oss.str());
}

const std::string& MissingNamedParameterError::get_missing_parameter_name() const noexcept
{
    return *missing_parameter_name;
}

boost::json::value MissingNamedParameterError::get_error_response_data() const noexcept
{
    return boost::json::object{{"missing_parameter_name", get_missing_parameter_name()}};
}

}
