#ifndef JSONRPC_SERVER_REQUEST_HPP
#define JSONRPC_SERVER_REQUEST_HPP

#include "jsonrpc/server/Error.hpp"

#include <boost/json.hpp>

#include <memory>
#include <string>
#include <exception>
#include <string_view>
#include <type_traits>

namespace jsonrpc::server {

class Request final {
  private:
    boost::json::object request;

  public:
    explicit Request(boost::json::object request);

    [[nodiscard]] const boost::json::string& get_method() const noexcept;
    [[nodiscard]] const boost::json::value* get_params() const noexcept;
    [[nodiscard]] const boost::json::value* get_id() const noexcept;

    [[nodiscard]] std::size_t get_number_of_parameters() const noexcept;

    [[nodiscard]] const boost::json::object& get_json() const& noexcept;
    [[nodiscard]] boost::json::object&& get_json() && noexcept;

  private:
    void validate_jsonrpc_member();
    void validate_method_member();
    void validate_params_member();
    void validate_id_member();
};

class RequestFormatError : public Error {
  private:
    std::shared_ptr<boost::json::object> request;

  public:
    boost::json::object& get_request() const noexcept;

  protected:
    RequestFormatError(boost::json::object request,
                       ErrorCode error_response_code,
                       std::string error_response_message) noexcept;

    RequestFormatError(const RequestFormatError& other) noexcept            = default;
    RequestFormatError& operator=(const RequestFormatError& other) noexcept = default;
    RequestFormatError(RequestFormatError&& other) noexcept                 = default;
    RequestFormatError& operator=(RequestFormatError&& other) noexcept      = default;
};

class MissingJsonRpcMemberRequestError final : public RequestFormatError {
  public:
    explicit MissingJsonRpcMemberRequestError(boost::json::object request) noexcept;
};

class InvalidJsonRpcMemberKindError final : public RequestFormatError {
  public:
    static constexpr boost::json::kind expected_kind{boost::json::kind::string};

  public:
    explicit InvalidJsonRpcMemberKindError(boost::json::object request) noexcept;

    [[nodiscard]] boost::json::kind get_json_rpc_member_kind() const noexcept;
    [[nodiscard]] boost::json::value get_error_response_data() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidJsonRpcMemberKindError>);

class InvalidJsonRpcMemberValueError final : public RequestFormatError {
  public:
    static constexpr std::string_view expected_value{"2.0"};

  public:
    explicit InvalidJsonRpcMemberValueError(boost::json::object request) noexcept;

    [[nodiscard]] std::string_view get_json_rpc_member_value() const noexcept;
    [[nodiscard]] boost::json::value get_error_response_data() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidJsonRpcMemberValueError>);

class MissingMethodMemberRequestError final : public RequestFormatError {
  public:
    explicit MissingMethodMemberRequestError(boost::json::object request) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<MissingMethodMemberRequestError>);

class InvalidMethodMemberKindError final : public RequestFormatError {
  public:
    static constexpr boost::json::kind expected_kind{boost::json::kind::string};

  public:
    explicit InvalidMethodMemberKindError(boost::json::object request) noexcept;

    [[nodiscard]] boost::json::kind get_method_member_kind() const noexcept;
    [[nodiscard]] boost::json::value get_error_response_data() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidMethodMemberKindError>);

class EmptyMethodMemberError final : public RequestFormatError {
  public:
    explicit EmptyMethodMemberError(boost::json::object request) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<EmptyMethodMemberError>);

class InvalidParamsMemberKindError final : public RequestFormatError {
  public:
    explicit InvalidParamsMemberKindError(boost::json::object request) noexcept;

    [[nodiscard]] boost::json::value& get_params() const noexcept;
    [[nodiscard]] boost::json::value get_error_response_data() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidParamsMemberKindError>);

class InvalidIdMemberKindError final : public RequestFormatError {
  public:
    explicit InvalidIdMemberKindError(boost::json::object request) noexcept;
    [[nodiscard]] boost::json::value& get_id() const noexcept;
    [[nodiscard]] boost::json::value get_error_response_data() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidIdMemberKindError>);

}

#endif
