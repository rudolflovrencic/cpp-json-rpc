#include "jsonrpc/client/Response.hpp"

#include <cassert>
#include <iomanip>
#include <sstream>

namespace jsonrpc::client {

Response::Response(boost::json::object json_object) : json_object{std::move(json_object)}
{
    validate_jsonrpc_member();
    validate_id_member();
}

const boost::json::value& Response::get_id() const noexcept
{
    return get_json().at("id");
}

boost::json::object& Response::get_json() noexcept
{
    return json_object;
}

const boost::json::object& Response::get_json() const noexcept
{
    return json_object;
}

void Response::validate_jsonrpc_member()
{
    const auto* const jsonrpc_it = json_object.find("jsonrpc");
    if (jsonrpc_it == json_object.end()) { throw MissingJsonRpcMemberResponseError{std::move(json_object)}; }

    const auto* const jsonrpc = jsonrpc_it->value().if_string();
    if (!jsonrpc) { throw InvalidJsonRpcMemberKindResponseError{std::move(json_object)}; }
    if (*jsonrpc != InvalidJsonRpcMemberValueResponseError::expected_value) {
        throw InvalidJsonRpcMemberValueResponseError{std::move(json_object)};
    }
}

void Response::validate_id_member()
{
    const auto* const id_it = json_object.find("id");
    if (id_it == json_object.end()) { throw MissingIdMemberResponseError{std::move(json_object)}; }

    const auto& id = id_it->value();
    if (!id.is_string() && !id.is_number() && !id.is_null()) {
        throw InvalidIdMemberKindResponseError{std::move(json_object)};
    }
}

SuccessResponse::SuccessResponse(boost::json::object success_response) : Response{std::move(success_response)}
{
    assert(get_json().contains("result"));
    assert(!get_json().contains("error"));
}

const boost::json::value& SuccessResponse::get_result() const noexcept
{
    return get_json().at("result");
}

boost::json::value& SuccessResponse::get_result() noexcept
{
    return get_json().at("result");
}

ErrorResponse::ErrorResponse(boost::json::object error_response) : Response{std::move(error_response)}
{
    assert(get_json().contains("error"));
    assert(!get_json().contains("result"));

    validate_error_member();
}

ErrorCode ErrorResponse::get_error_code() const noexcept
{
    return static_cast<ErrorCode>(get_error_object().at("code").as_int64());
}

const boost::json::string& ErrorResponse::get_error_message() const noexcept
{
    return get_error_object().at("message").as_string();
}

boost::json::string& ErrorResponse::get_error_message() noexcept
{
    return get_error_object().at("message").as_string();
}

const boost::json::value* ErrorResponse::get_error_data() const noexcept
{
    return get_error_object().if_contains("data");
}

boost::json::value* ErrorResponse::get_error_data() noexcept
{
    return get_error_object().if_contains("data");
}

const boost::json::object& ErrorResponse::get_error_object() const noexcept
{
    return get_json().at("error").as_object();
}

boost::json::object& ErrorResponse::get_error_object() noexcept
{
    return get_json().at("error").as_object();
}

void ErrorResponse::validate_error_member()
{
    const auto& error           = get_json().at("error");
    const auto* const error_obj = error.if_object();
    if (!error_obj) { throw InvalidErrorMemberKindResponseError{std::move(get_json())}; }

    const auto* const error_code = error_obj->if_contains("code");
    if (!error_code) { throw ErrorObjectMissingCodeMemberResponseError{std::move(get_json())}; }
    if (!error_code->is_int64() && !error_code->is_uint64()) {
        throw ErrorObjectInvalidCodeMemberKindResponseError{std::move(get_json())};
    }

    const auto* const message = error_obj->if_contains("message");
    if (!message) { throw ErrorObjectMissingMessageMemberResponseError{std::move(get_json())}; }
    if (!message->is_string()) { throw ErrorObjectInvalidMessageMemberKindResponseError{std::move(get_json())}; }
}

std::vector<std::variant<SuccessResponse, ErrorResponse>> parse_response(std::string_view response)
{
    std::error_code ec;
    auto json_value = boost::json::parse(response, ec);
    if (ec) { throw ResponseStringParseError{std::string{response}}; }
    return parse_response(std::move(json_value));
}

std::vector<std::variant<SuccessResponse, ErrorResponse>> parse_response(boost::json::value response)
{
    if (auto* const response_obj = response.if_object()) {
        return {parse_response(std::move(*response_obj))};
    } else if (auto* const response_arr = response.if_array()) {
        if (response_arr->empty()) { throw EmptyBatchResponseError{}; }
        std::vector<std::variant<SuccessResponse, ErrorResponse>> result;
        result.reserve(response_arr->size());
        for (std::size_t i{0}; i < response_arr->size(); i++) {
            auto* const response_obj = (*response_arr)[i].if_object();
            if (!response_obj) { throw InvalidResponseArrayMemberKindError{std::move(*response_arr), i}; }
            result.push_back(parse_response(std::move(*response_obj)));
        }
        return result;
    } else {
        throw InvalidResponseKindError{std::move(response)};
    }
}

std::variant<SuccessResponse, ErrorResponse> parse_response(boost::json::object response)
{
    if (response.contains("result")) {
        if (response.contains("error")) { throw ContainingResultAndErrorMembersResponseError{std::move(response)}; }
        return SuccessResponse{std::move(response)};
    } else if (response.contains("error")) {
        return ErrorResponse{std::move(response)};
    } else {
        throw MissingResultAndErrorMembersResponseError{std::move(response)};
    }
}

ResponseStringParseError::ResponseStringParseError(std::string response_string) noexcept
        : response_string{std::make_shared<std::string>(std::move(response_string))}
{
    std::ostringstream oss;
    oss << "Response string is not a valid JSON. Response string: " << get_response_string();
    set_what_message(oss.str());
}

const std::string& ResponseStringParseError::get_response_string() const noexcept
{
    return *response_string;
}

boost::json::object& ResponseFormatError::get_response() const noexcept
{
    return *response;
}

InvalidResponseKindError::InvalidResponseKindError(boost::json::value response_json_value) noexcept
        : response_json_value{std::make_shared<boost::json::value>(std::move(response_json_value))}
{
    assert(get_response_kind() != boost::json::kind::object && get_response_kind() != boost::json::kind::array);
    std::ostringstream oss;
    oss << "Invalid JSON-RPC response kind.\nExpected: " << boost::json::kind::array << " or "
        << boost::json::kind::object << "\nEncountered: " << get_response_kind()
        << "\nResponse : " << get_response_json_value();
    set_what_message(oss.str());
}

boost::json::kind InvalidResponseKindError::get_response_kind() const noexcept
{
    return get_response_json_value().kind();
}

const boost::json::value& InvalidResponseKindError::get_response_json_value() const noexcept
{
    return *response_json_value;
}

InvalidResponseArrayMemberKindError::InvalidResponseArrayMemberKindError(boost::json::array response_json_array,
                                                                         std::size_t invalid_member_index) noexcept
        : response_json_array{std::make_shared<boost::json::array>(std::move(response_json_array))},
          invalid_member_index{invalid_member_index}
{
    assert(invalid_member_index < get_response_json_array().size());
    assert(get_invalid_member_kind() != expected_kind);
    std::ostringstream oss;
    oss << "Invalid JSON-RPC array response member kind (index " << get_invalid_member_index()
        << ").\nExpected: " << expected_kind << "\nEncountered: " << get_invalid_member_kind()
        << "\nResponse : " << get_response_json_array();
    set_what_message(oss.str());
}

std::size_t InvalidResponseArrayMemberKindError::get_invalid_member_index() const noexcept
{
    return invalid_member_index;
}

boost::json::kind InvalidResponseArrayMemberKindError::get_invalid_member_kind() const noexcept
{
    return get_response_json_array()[get_invalid_member_index()].kind();
}

const boost::json::array& InvalidResponseArrayMemberKindError::get_response_json_array() const noexcept
{
    return *response_json_array;
}

EmptyBatchResponseError::EmptyBatchResponseError() noexcept
{
    set_what_message("Empty batch response encountered.");
}

ResponseFormatError::ResponseFormatError(boost::json::object response) noexcept
        : response{std::make_shared<boost::json::object>(std::move(response))}
{}

MissingJsonRpcMemberResponseError::MissingJsonRpcMemberResponseError(boost::json::object response) noexcept
        : ResponseFormatError{std::move(response)}
{
    assert(!get_response().contains("jsonrpc"));
    std::ostringstream oss;
    oss << "Missing mandatory response member \"jsonrpc\".\nResponse: " << get_response();
    set_what_message(oss.str());
}

InvalidJsonRpcMemberKindResponseError::InvalidJsonRpcMemberKindResponseError(boost::json::object response) noexcept
        : ResponseFormatError{std::move(response)}
{
    assert(get_json_rpc_member_kind() != expected_kind);
    std::ostringstream oss;
    oss << "Invalid kind for response member \"jsonrpc\".\nExpected: " << expected_kind
        << "\nEncountered: " << get_json_rpc_member_kind() << "\nResponse: " << get_response();
    set_what_message(oss.str());
}

boost::json::kind InvalidJsonRpcMemberKindResponseError::get_json_rpc_member_kind() const noexcept
{
    assert(get_response().contains("jsonrpc"));
    return get_response().if_contains("jsonrpc")->kind();
}

InvalidJsonRpcMemberValueResponseError::InvalidJsonRpcMemberValueResponseError(boost::json::object response) noexcept
        : ResponseFormatError{std::move(response)}
{
    assert(get_json_rpc_member_value() != expected_value);
    std::ostringstream oss;
    oss << "Invalid value for response member \"jsonrpc\".\nExpected: " << std::quoted(expected_value)
        << "\nEncountered: " << std::quoted(get_json_rpc_member_value()) << "\nResponse: " << get_response();
}

std::string_view InvalidJsonRpcMemberValueResponseError::get_json_rpc_member_value() const noexcept
{
    assert(get_response().contains("jsonrpc"));
    assert(get_response().if_contains("jsonrpc")->is_string());
    return get_response().if_contains("jsonrpc")->if_string()->c_str();
}

MissingIdMemberResponseError::MissingIdMemberResponseError(boost::json::object response) noexcept
        : ResponseFormatError{std::move(response)}
{
    assert(!get_response().contains("id"));
    std::ostringstream oss;
    oss << "Missing mandatory response member \"id\".\nResponse: " << get_response();
    set_what_message(oss.str());
}

InvalidIdMemberKindResponseError::InvalidIdMemberKindResponseError(boost::json::object response) noexcept
        : ResponseFormatError{std::move(response)}
{
    assert(!get_id().is_number() && !get_id().is_string() && !get_id().is_null());
    std::ostringstream oss;
    oss << "Invalid kind for response member \"id\".\nExpected: number, string "
           "or null\nEncountered: "
        << get_id().kind() << "\nResponse : " << get_response();
    set_what_message(oss.str());
}

boost::json::value& InvalidIdMemberKindResponseError::get_id() const noexcept
{
    assert(get_response().contains("id"));
    return get_response().at("id");
}

MissingResultAndErrorMembersResponseError::MissingResultAndErrorMembersResponseError(
    boost::json::object response) noexcept
        : ResponseFormatError{std::move(response)}
{
    assert(!get_response().contains("result") && !get_response().contains("error"));
    std::ostringstream oss;
    oss << "Reponse does not contain \"result\" nor \"error\" "
           "member.\nResponse: "
        << get_response();
    set_what_message(oss.str());
}

ContainingResultAndErrorMembersResponseError::ContainingResultAndErrorMembersResponseError(
    boost::json::object response) noexcept
        : ResponseFormatError{std::move(response)}
{
    assert(get_response().contains("result") && get_response().contains("error"));
    std::ostringstream oss;
    oss << "Reponse contains both \"result\" and \"error\" members.\nResponse: " << get_response();
    set_what_message(oss.str());
}

InvalidErrorMemberKindResponseError::InvalidErrorMemberKindResponseError(boost::json::object response) noexcept
        : ResponseFormatError{std::move(response)}
{
    assert(get_error_member_kind() != expected_kind);
    std::ostringstream oss;
    oss << "Invalid kind for response member \"error\".\nExpected: " << expected_kind
        << "\nEncountered: " << get_error_member_kind() << "\nResponse: " << get_response();
    set_what_message(oss.str());
}

boost::json::kind InvalidErrorMemberKindResponseError::get_error_member_kind() const noexcept
{
    assert(get_response().contains("error"));
    return get_response().if_contains("error")->kind();
}

ResponseErrorObjectFormatError::ResponseErrorObjectFormatError(boost::json::object response) noexcept
        : ResponseFormatError{std::move(response)}
{
    assert(get_response().contains("error"));
    assert(get_response().at("error").is_object());
}

boost::json::object& ResponseErrorObjectFormatError::get_error_object() const noexcept
{
    return get_response().at("error").get_object();
}

ErrorObjectMissingCodeMemberResponseError::ErrorObjectMissingCodeMemberResponseError(
    boost::json::object response) noexcept
        : ResponseErrorObjectFormatError{std::move(response)}
{
    assert(!get_error_object().contains("code"));
    std::ostringstream oss;
    oss << "Missing mandatory error response member \"error.code\".\nResponse: " << get_response();
    set_what_message(oss.str());
}

ErrorObjectInvalidCodeMemberKindResponseError::ErrorObjectInvalidCodeMemberKindResponseError(
    boost::json::object response) noexcept
        : ResponseErrorObjectFormatError{std::move(response)}
{
    assert(get_error_code_member_kind() != boost::json::kind::int64 &&
           get_error_code_member_kind() != boost::json::kind::uint64);
    std::ostringstream oss;
    oss << "Invalid error response member \"error.code\" kind.\nExpected: "
           "integer\nEncountered: "
        << get_error_code_member_kind() << "\nResponse: " << get_response();
    set_what_message(oss.str());
}

boost::json::kind ErrorObjectInvalidCodeMemberKindResponseError::get_error_code_member_kind() const noexcept
{
    assert(get_error_object().contains("code"));
    return get_error_object().at("code").kind();
}

ErrorObjectMissingMessageMemberResponseError::ErrorObjectMissingMessageMemberResponseError(
    boost::json::object response) noexcept
        : ResponseErrorObjectFormatError{std::move(response)}
{
    assert(!get_error_object().contains("message"));
    std::ostringstream oss;
    oss << "Missing mandatory error response member "
           "\"error.message\".\nResponse: "
        << get_response();
    set_what_message(oss.str());
}

ErrorObjectInvalidMessageMemberKindResponseError::ErrorObjectInvalidMessageMemberKindResponseError(
    boost::json::object response) noexcept
        : ResponseErrorObjectFormatError{std::move(response)}
{
    assert(get_error_message_member_kind() != expected_kind);
    std::ostringstream oss;
    oss << "Invalid error response member \"error.message\" kind.\nExpected: " << expected_kind
        << "\nEncountered: " << get_error_message_member_kind() << "\nResponse: " << get_response();
    set_what_message(oss.str());
}

boost::json::kind ErrorObjectInvalidMessageMemberKindResponseError::get_error_message_member_kind() const noexcept
{
    assert(get_error_object().contains("message"));
    return get_error_object().at("message").kind();
}
}
