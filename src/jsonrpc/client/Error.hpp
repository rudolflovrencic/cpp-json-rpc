#ifndef JSONRPC_CLIENT_ERROR_HPP
#define JSONRPC_CLIENT_ERROR_HPP

#include <boost/json.hpp>

#include <string>
#include <memory>

namespace jsonrpc::client {

class Error : public std::exception {
  private:
    std::shared_ptr<std::string> what_message;

  public:
    ~Error() noexcept override = default;

    [[nodiscard]] const char* what() const noexcept final;

  protected:
    Error() noexcept;
    void set_what_message(std::string set_what_message) const noexcept;

    Error(const Error& other) noexcept            = default;
    Error& operator=(const Error& other) noexcept = default;
    Error(Error&& other) noexcept                 = default;
    Error& operator=(Error&& other) noexcept      = default;
};

}

#endif
