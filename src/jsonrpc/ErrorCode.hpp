#ifndef JSONRPC_ERROR_HPP
#define JSONRPC_ERROR_HPP

namespace jsonrpc {

enum class ErrorCode : int {
    ParseError     = -32700,
    InvalidRequest = -32600,
    MethodNotFound = -32601,
    InvalidParams  = -32602,
    InternalError  = -32603
};

}

#endif
