#ifndef JSONRPC_SERVER_SERVER_HPP
#define JSONRPC_SERVER_SERVER_HPP

#include "jsonrpc/server/Request.hpp"
#include "jsonrpc/server/Error.hpp"
#include "jsonrpc/server/Handler.hpp"

#include <boost/json.hpp>

#include <memory>
#include <string>
#include <variant>
#include <exception>
#include <optional>
#include <functional>
#include <type_traits>
#include <string_view>
#include <unordered_set>

namespace jsonrpc::server {

class Server final {
  private:
    using TypeErasedHandler = std::function<boost::json::value(Request request)>;

  private:
    std::unordered_map<std::string, TypeErasedHandler> handlers;

  public:
    template<typename Ret, typename... Args>
    void add(std::string method, std::function<Ret(Args...)> handler, std::vector<std::string> parameter_names);

    template<typename Callable>
    void add(std::string method, Callable handler, std::vector<std::string> parameter_names);

    [[nodiscard]] std::optional<boost::json::object> handle_request(boost::json::object request_json) const;

    [[nodiscard]] std::optional<boost::json::value> handle_request(boost::json::array batch_request_json) const;

    [[nodiscard]] std::optional<std::string> parse_and_handle_request(std::string_view request_string) const;

  private:
    [[nodiscard]] std::unordered_set<std::string> get_registered_method_names() const noexcept;

    static void validate_parameter_names(std::string_view method,
                                         std::size_t n_parameters,
                                         const std::vector<std::string>& parameter_names);
};

static constexpr std::string_view reserved_method_name_prefix{"rpc."};

class MethodRegistrationError : public std::exception {
  private:
    std::shared_ptr<std::string> what_message;
    std::shared_ptr<std::string> method_name;

  public:
    [[nodiscard]] const char* what() const noexcept final;
    [[nodiscard]] const std::string& get_method_name() const noexcept;

  protected:
    explicit MethodRegistrationError(std::string method_name) noexcept;
    void set_what_message(std::string what_message) const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<MethodRegistrationError>);

class ReservedMethodNameError final : public MethodRegistrationError {
  public:
    explicit ReservedMethodNameError(std::string method_name) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<ReservedMethodNameError>);

class IncorrectNumberOfParameterNamesError final : public MethodRegistrationError {
  private:
    std::size_t n_parameters;
    std::size_t n_parameter_names;

  public:
    IncorrectNumberOfParameterNamesError(std::string method_name,
                                         std::size_t n_parameters,
                                         std::size_t n_parameter_names) noexcept;

    [[nodiscard]] std::size_t get_number_of_parameters() const noexcept;
    [[nodiscard]] std::size_t get_number_of_parameter_names() const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<IncorrectNumberOfParameterNamesError>);

class EmptyParameterNameError final : public MethodRegistrationError {
  public:
    explicit EmptyParameterNameError(std::string method) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<EmptyParameterNameError>);

class DuplicateParameterNameError final : public MethodRegistrationError {
  private:
    std::shared_ptr<std::string> duplicate_parameter_name;

  public:
    DuplicateParameterNameError(std::string method_name, std::string duplicate_parameter_name) noexcept;
    [[nodiscard]] const std::string& get_duplicate_parameter_name() const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<DuplicateParameterNameError>);

class MethodNotFoundError final : public RequestSemanticError {
  private:
    std::shared_ptr<std::unordered_set<std::string>> registered_method_names;

  public:
    MethodNotFoundError(Request request, std::unordered_set<std::string> registered_method_names) noexcept;

    [[nodiscard]] std::string_view get_method() const noexcept;
    [[nodiscard]] std::unordered_set<std::string>& get_registered_method_names() const noexcept;
    [[nodiscard]] boost::json::value get_error_response_data() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<MethodNotFoundError>);

template<typename Ret, typename... Args>
void Server::add(std::string method, std::function<Ret(Args...)> handler, std::vector<std::string> parameter_names)
{
    if (method.rfind(reserved_method_name_prefix, 0) == 0) { throw ReservedMethodNameError{std::move(method)}; }

    validate_parameter_names(method, sizeof...(Args), parameter_names);

    handlers[std::move(method)] = Handler{handler, std::move(parameter_names)};
}

template<typename Callable>
void Server::add(std::string method, Callable handler, std::vector<std::string> parameter_names)
{
    add(std::move(method), std::function{std::forward<Callable>(handler)}, std::move(parameter_names));
}

}

#endif
