#include "jsonrpc/server/Error.hpp"

#include <cassert>

namespace jsonrpc::server {

boost::json::object create_error_response(ErrorCode error_code,
                                          std::string message,
                                          boost::json::value data,
                                          boost::json::value id) noexcept
{
    assert(!message.empty());
    assert(id.is_number() || id.is_string() || id.is_null());

    boost::json::object error{{"code", static_cast<int>(error_code)}, {"message", std::move(message)}};
    if (!data.is_null()) { error["data"] = std::move(data); }

    return boost::json::object{{"jsonrpc", "2.0"}, {"error", std::move(error)}, {"id", std::move(id)}};
}

Error::Error(ErrorCode error_response_code, std::string error_response_message) noexcept
        : what_message{std::make_shared<std::string>()},
          error_response_code{error_response_code},
          error_response_message{std::make_shared<std::string>(std::move(error_response_message))}
{
    assert(!get_error_response_message().empty());
}

ErrorCode Error::get_error_response_code() const noexcept
{
    return error_response_code;
}

const std::string& Error::get_error_response_message() const noexcept
{
    return *error_response_message;
}

boost::json::value Error::get_error_response_data() const noexcept
{
    return boost::json::value{};
}

boost::json::object Error::create_error_response(boost::json::value id) const noexcept
{
    return server::create_error_response(
        get_error_response_code(), get_error_response_message(), get_error_response_data(), std::move(id));
}

const char* Error::what() const noexcept
{
    assert(!what_message->empty());
    return what_message->c_str();
}

void Error::set_what_message(std::string what_message) const noexcept
{
    assert(!what_message.empty());
    *this->what_message = what_message;
}

}
